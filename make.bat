::version 2.10
@echo off
setlocal enableextensions enabledelayedexpansion
SET swpath=w:\DEV\Users\Benjamin\p27463-conv
SET perlapp="C:\ActiveState Perl Dev Kit 9.5.1\bin\perlapp.exe"
SET drive=%CD:~0,2%

PUSHD %swpath%\conv
w:

:compile
FOR /d %%X IN (genfilter1 genfilter2 genfilter3 genfilter4 premap premap2 final) DO (
	x:\USERS\Benjamin\sw\filecmp %%X.pl %%X.exe
	IF !ERRORLEVEL! EQU 104 goto error
	IF !ERRORLEVEL! EQU 1 (
		%perlapp% --script %%X.pl      --exe %%X.exe      --freestanding --force  --norunlib  --lib y:\sw\lib\perl
	) ELSE (
		ECHO not compiling %%X because no changes to pl since last compilation
	)
)
::        copy %%X.exe w:\sw\p27463\conv\%%X.exe
cd ../pack

FOR /d %%X IN (packsw) DO (
	x:\USERS\Benjamin\sw\filecmp %%X.pl %%X.exe
	IF !ERRORLEVEL! EQU 104 goto error
	IF !ERRORLEVEL! EQU 1 (
		%perlapp% --script %%X.pl      --exe %%X.exe      --freestanding --force  --norunlib  --lib y:\sw\lib\perl Y:\SRCCTL\PERL\LIB

	) ELSE (
		ECHO not compiling %%X because no changes to pl since last compilation
	)
)
::        copy %%X.exe w:\sw\p27463\pack\%%X.exe

%drive%
POPD

copy w:\DEV\Users\Benjamin\p27463-conv\conv\*.tbl w:\sw\p27463\conv\*.tbl
copy w:\DEV\Users\Benjamin\p27463-conv\conv\*.map w:\sw\p27463\conv\*.map
copy w:\DEV\Users\Benjamin\p27463-conv\conv\*.fnl w:\sw\p27463\conv\*.fnl
copy w:\DEV\Users\Benjamin\p27463-conv\filters.bat w:\sw\p27463\filters.bat
copy w:\DEV\Users\Benjamin\p27463-conv\upsproc.bat w:\sw\p27463\upsproc.bat

goto end

:error
echo An error occured!


:end
