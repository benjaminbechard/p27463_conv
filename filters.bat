setlocal enableextensions enabledelayedexpansion
set map_path=y:\sw\p27463\conv
set exitmsg="failure in document conversion"
set DOC=%1
set TEST=%2
set PID=p27463
set swpath=w:\sw\%PID%
set devpath=W:\DEV\Users\Benjamin\%PID%-conv

::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:rainbow
call p:\grig\off7\bat\docxrbwallatr.bat %DOC%




::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:utf8
::p:\grig\pl\xml\ucode\utf8_htm  %DOC%.rbw -o%DOC%.f10
p:\grig\wpb\ucode\mathml\isou16 -c -my:\sw\P25912\VIEWER\docbkmth.lst %DOC%.rbw -o%DOC%.f10
IF NOT EXIST %DOC%.f10 (
	set exitmsg="Error in clean step. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)






::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:premap
IF {%TEST%}=={testpl} (
    perl %devpath%\conv\premap.pl  --input=%DOC%.f10 --output=%DOC%.f15
) ELSE (
         %swpath%\conv\premap.exe --input=%DOC%.f10 --output=%DOC%.f15
)
IF errorlevel 104 (
	set exitmsg="Error in premap step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f15 (
	set exitmsg="Error in premap step. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)



IF {%TEST%}=={testpl} (
    perl %devpath%\conv\premap2.pl  --input=%DOC%.f15 --output=%DOC%.f20
) ELSE (
         %swpath%\conv\premap2.exe --input=%DOC%.f15 --output=%DOC%.f20
)
IF errorlevel 104 (
	set exitmsg="Error in premap2 step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f20 (
	set exitmsg="Error in premap2 step. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)




if {%Section2%}=={2} goto map2
::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:map
IF {%TEST%}=={testpl} (
        %devpath%\conv\rbf_ind -m%devpath%\conv\ind.fnl -s%devpath%\conv\ind.tbl %DOC%.f20 > %DOC%.f25
) ELSE (
         %swpath%\conv\rbf_ind -m%swpath%\conv\ind.fnl -s%swpath%\conv\ind.tbl %DOC%.f20 > %DOC%.f25
)
IF errorlevel 103 (
	set exitmsg="Error in map step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f25 (
	set exitmsg="Error in map step. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
goto CONVERSION


::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:map2
IF {%TEST%}=={testpl} (
        %devpath%\conv\rbf_ind -m%devpath%\conv\ind2.fnl -s%devpath%\conv\ind2.tbl %DOC%.f20 > %DOC%.f25
) ELSE (
         %swpath%\conv\rbf_ind -m%swpath%\conv\ind2.fnl -s%swpath%\conv\ind2.tbl %DOC%.f20 > %DOC%.f25
)
IF errorlevel 103 (
	set exitmsg="Error in map step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f25 (
	set exitmsg="Error in map step. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
goto CONVERSION




:CONVERSION

::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:c1
IF {%TEST%}=={testpl} (
    perl %devpath%\conv\genfilter1.pl  --input=%DOC%.f25 --output=%DOC%.f30
) ELSE (
         %swpath%\conv\genfilter1.exe --input=%DOC%.f25 --output=%DOC%.f30
)
IF errorlevel 104 (
	set exitmsg="Error in c1 step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f30 (
	set exitmsg="Error in c1. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)



::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:c2
IF {%TEST%}=={testpl} (
    perl %devpath%\conv\genfilter2.pl  --input=%DOC%.f30 --output=%DOC%.f35
) ELSE (
         %swpath%\conv\genfilter2.exe --input=%DOC%.f30 --output=%DOC%.f35
)
IF errorlevel 104 (
	set exitmsg="Error in c2 step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f35 (
	set exitmsg="Error in c2. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)






::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:c3
IF {%TEST%}=={testpl} (
    perl %devpath%\conv\genfilter3.pl  --input=%DOC%.f35 --output=%DOC%.f40
) ELSE (
         %swpath%\conv\genfilter3.exe --input=%DOC%.f35 --output=%DOC%.f40
)
IF errorlevel 104 (
	set exitmsg="Error in c3 step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f40 (
	set exitmsg="Error in c3. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)





::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:c4
IF {%TEST%}=={testpl} (
    perl %devpath%\conv\genfilter4.pl  --input=%DOC%.f40 --output=%DOC%.f45
) ELSE (
         %swpath%\conv\genfilter4.exe --input=%DOC%.f40 --output=%DOC%.f45
)
IF errorlevel 104 (
	set exitmsg="Error in c4 step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
IF NOT EXIST %DOC%.f45 (
	set exitmsg="Error in c4. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)






::~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:final
IF {%TEST%}=={testpl} (
    perl %devpath%\conv\final.pl  --input=%DOC%.f45 --output=%DOC%.f95
) ELSE (
         %swpath%\conv\final.exe --input=%DOC%.f45 --output=%DOC%.f95
)
IF errorlevel 104 (
	set exitmsg="Error in final step"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)
if {%Section2%}=={2} goto cleanup
IF NOT EXIST *.f95 (
	set exitmsg="Error in final. Output not created"
	echo "Error for [[%DOC%]]. With error message: %exitmsg%" >> error.err
    goto error
)





:xml
copy *.f95 *.xml
del *.f95

:cleanup

IF NOT {%TEST%}=={testpl} (
    if exist %DOC%.f* del /Q %DOC%.f*
    if exist %DOC%.rbw del /Q %DOC%.rbw
    if exist %DOC%.skp del /Q %DOC%.skp
    if exist %DOC%.lst del /Q %DOC%.lst
)


if exist error.err goto error
set exitmsg="Successfully converted document"
goto done


:error
echo "Errors found during conversion. Check %DOC%.elg file for full list."
goto done


:done
ECHO %exitmsg%
