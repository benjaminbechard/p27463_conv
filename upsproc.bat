@echo on
setlocal enableextensions enabledelayedexpansion
::version 2
::param 1 - publication
::param 2 - Unit Class ID ()
::param 3 - Process Id
::if {%3}=={} goto usage
set stfile=errstat.txt
set automate=1
set PID=p27463
set prodsw=w:\sw\%PID%
set testsw=X:\DEV\Users\Benjamin\p27463-conv
set stateid=0
set TEST=
set Section2=1

IF {%1}=={testpl} (
	SET TEST=testpl
)
IF {%2}=={testpl} (
	SET TEST=testpl
)


IF {%1}=={2} (
	SET Section2=2
)
IF {%2}=={2} (
	SET Section2=2
)

::---------Project Info----------
set drive=K
set statmsg="Did Not Run through Conversion!"
set statnum=300
set statdir=c:\pcs_a_temp
set statext=pcs
set statfile=%statdir%\%stateid%.%statext%
::---------------------------------------------


:start
if exist %statfile% del /Q %statfile%
if exist %stfile% del /Q %stfile%
if exist docx.lst del /Q docx.lst
if exist error.err del /Q error.err
if exist *.xml del /Q *.xml
if exist %DOC%.f* del /Q %DOC%.f*
if exist %DOC%.rbw del /Q %DOC%.rbw
if exist %DOC%.skp del /Q %DOC%.skp
if exist %DOC%.lst del /Q %DOC%.lst
if exist programming_error.elg del /Q programming_error.elg

::del /Q *.f*

::\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
::\\\\\\\\\\STEP 1 \\\\\\\\\\\\\\\\
:step1
%drive%:

::IF {%pub%}=={} (
::	SET fldir=%CD%
::	x:\bin\pgm\autochkdir -p"!fldir!"
::	IF errorlevel 103 (
::		SET statmsg="[[%pub%]] was specified but %pub%\work\rtf\ was not found."
::		GOTO error
::	)
::	goto step2
::)
::
::SET fldir=\%PID%\Active\%pub%\work\rtf
::IF NOT {%pub%}=={} (
::	CD !fldir!
::	x:\bin\pgm\autochkdir -p"!fldir!"
::	IF errorlevel 103 (
::		SET statmsg="[[%pub%]] was specified but %pub%\work\rtf\ was not found."
::		GOTO error
::	)
::)

::\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
::\\\\\\\\\\\\\ STEP 2 \\\\\\\\\\\
:step2
IF {%TEST%}=={testpl} (
    perl Y:\USERS\Benjamin\sw\doc\doc.pl docx
) ELSE (
         Y:\USERS\Benjamin\sw\doc\doc.exe docx
)
IF errorlevel 104 (
	SET statmsg="error creating docx list"
	GOTO error
)
IF NOT EXIST docx.lst (
	SET statmsg="docx.lst missing or no docx files found in current folder"
	GOTO error
)


::\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
::\\\\\\\\\\\\\ STEP 3 \\\\\\\\\\\
:step3
for /F "tokens=*" %%A in (docx.lst) do (
	echo converting %%A
	copy "%%A.docx" "conv.docx"
	IF {%TEST%}=={testpl} (
		call %testsw%\filters.bat conv %TEST%
	) ELSE (
		call %prodsw%\filters.bat conv %TEST%
	)
	del conv.docx

)
if exist error.err (
	SET statmsg="The conversion process returned an error."
	GOTO error
)



::\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
::\\\\\\\\\\\\\ STEP 4 \\\\\\\\\\\
:step4






::\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
::\\\\\\\\\\\\\ STEP  5 \\\\\\\\\\\
:step5
::call K:\P27313\sw\sparse.bat   %unitname%
::if errorlevel 104 goto parserr
::if errorlevel 103 goto parserr




if exist error.err GOTO error



::if exist *.f* del /Q *.f*
::if exist *.rbw del /Q *.rbw
if exist *.skp del /Q *.skp
if exist *.lst del /Q *.lst
if exist rename.txt del /Q rename.txt
goto success



:success
set statmsg="Succesful! - converted"
set statnum=100



goto pcsproc


::\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
::\\\\\\\\\\\ EXIT CODES \\\\\\\\\
:error
set statnum=316
goto pcsproc
::
:usage
set statmsg="Missing pcsid"
set statnum=201
goto pcsproc
:nodir
set statmsg="Could NOT find %fldir%"
set statnum=212
goto pcsproc
::
:pcsproc
::w:\bin\setup\autostat -m%statmsg% -e%statnum%  -o%statfile%


:done
@echo off
if {%statnum%}=={100} (
	set statmsg=
	set statnum=
	set statdir=
	set statext=
	set statfile=
	set nlmproj=
	echo done
	echo. && echo. && echo %statmsg% && echo. && echo.
) else (
	echo. && echo. && echo %statmsg% && echo. && echo.
	if exist error.err (echo. && echo. && type error.err && echo. && echo.)
)
@echo on
