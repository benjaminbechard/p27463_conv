#!perl
use strict;

use File::Basename;
use File::Path qw(make_path);
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use File::Find::Rule;
use Data::GUID;

use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;

use lib 'Y:\SRCCTL\PERL\LIB';
	use DCL::Genline;
	use DCL::Zipctl;

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $genlinePM = DCL::Genline->new('Dcl Genline','indesign_p27463');
my $zipctlPM = DCL::Zipctl->new('Dcl Zipctl','statedep');
my $errorPM;
my $highest_error_level = 100;
$|=1;
my %ID;
my %GUID;
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c=<>if$DEBUG};

{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %IDHASH = ();
my @foundfiles = ();
my $elg = 'packup.elg';
my $procerr = 0;
my %ZIPHASH = ();
my %packhash = ();
my %alltopics = ();
my %IMAGES = ();



	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
		$SETUP{'home'} = $path;
		$SETUP{'home'} =~ s#/#\\#sg;
		if ($SETUP{'home'} =~ m#[a-z]:[\\/]P[0-9]+[\\/]active[\\/]([^\\/]+)#si) {
			$SETUP{'unit'} = $1;
			say STDERR "Unit: ".$SETUP{'unit'} if defined $SETUP{'unit'};
		}
		if (!$SETUP{'unit'}) {
			die( 'could not determine unit name ['.$SETUP{'home'}.']');
		}
		my @xmls = <*.xml>;my $zipname = $xmls[0]; $zipname =~ s#[.]xml##;
		$SETUP{'zip'} = $zipname;
		$SETUP{'pack'} = $SETUP{'home'}.'\pack';
		$SETUP{'temp'} = $SETUP{'pack'}.'\_temp';
		$SETUP{'zipctl'} = $SETUP{'pack'}.'\zipinfo.ctl';

	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();
	if (-e $elg) {
		unlink($elg);
        }

	$errorPM = log::error->new("Error Log File",$elg,$Version);

	&step0($SETUP{'pack'},'onlymd');
	&step0($SETUP{'temp'},'onlymd');
	if (-e $SETUP{'zipctl'}) {
		unlink($SETUP{'zipctl'});
	}
	my %xmlhash = ();
	my (@xmlarray) = $commonPM->getdirfiles_hash($SETUP{'home'},{'ext' => 'xml'});
	if (!@xmlarray) {
		$errorPM->error({
			type=>'fatal',
			msg=>"could NOT find any xml files"
		});
	}
	else {
		#if (scalar(@xmlarray) > 1) {
			#$errorPM->error({
			#	type=>'fatal',
			#	msg=>"found multiple xml files"
			#});
		#}
		#else {
		#	$xmlhash{$xmlarray[0]} = 1;
		#}
		#
		%xmlhash = map {$_ => 1} @xmlarray;
	}
	foreach my $xml (sort(keys(%xmlhash))) {
		$errorPM->error({
			type=>'info',
			msg=>"------ $xml -----"
		});
		my ($basename,$path,$suffix) = fileparse($xml,('.xml'));
		my $data = io($xml)->utf8->slurp;
		$data = &editfile($data);

		my $tmpfile = $SETUP{'temp'}.'\\'.$basename.$suffix;
		if (-e $tmpfile) {
			unlink($tmpfile);
        }
		$errorPM->error({
			type=>'info',
			msg=>"fixing up linebreaks..."
		});
		$data = $genlinePM->applyxml($data);
		$errorPM->error({
			type=>'info',
			msg=>"writing $tmpfile"
		});
		io($tmpfile)->utf8->write($data);
		my $zipfile = $xml;
		$ZIPHASH{lc($tmpfile)}{'zipup'} = $zipfile;
		$errorPM->error({
			type=>'info',
			msg=>"-------------------"
		});


	}

	#if(-e "images" and -d "images") {
	#	chdir "images";
	#	foreach my $img (<*.*>) {
	#		$IMAGES{$img} = 1;
	#	}
	#	chdir "..";
	#}

	if (%IMAGES) {
		mkdir $SETUP{'temp'}.'\\images' if not -e $SETUP{'temp'}.'\\images';
		my $dir = getcwd();
		foreach my $img (sort(keys(%IMAGES))) {
			my $tmpfile = $SETUP{'temp'}.'\\images\\'.$img;
			if (-e $tmpfile) {
				unlink($tmpfile);
			}
			File::Copy::copy("$dir/images/$img",$tmpfile);
			if($IMAGES{$img} == 1) {
				$ZIPHASH{lc('images/'.$img)}{'zipup'} = "images/$img";
				$errorPM->error({
					type=>'info',
					msg=>"processing [$img]"
				});
			} else {
				print "\n\n\n\n\n!!!!!!!!!!!!!!!!!!!!!!!\n\n\n\n";
				$errorPM->error({
					type=>'fatal',
					msg=>"image [$img] is missing"
				});
				print "\n\n\n\n\n!!!!!!!!!!!!!!!!!!!!!!!\n\n\n\n";
			}
		}
	}

	if ($procerr) {
		$errorPM->error({
			type=>'fatal',
			msg=>"above error(s) must be addressed before continuing #1"
		});
        }

	{
		foreach my $img (sort(keys(%IMAGES))) {
			#$ZIPHASH{lc($img)}{'zipup'} = $IMAGES{$img};
		}
	}

	&zipctl();

	if ($procerr) {
		$errorPM->error({
			type=>'fatal',
			msg=>"above error(s) must be addressed before continuing #3"
		});
        }


	$errorPM->finish(1);
	#-------------------------------------------------

	sub editfile {
		my ($txt) = @_;

		$txt =~ s#<p>[\s]+#<p>#g;
		$txt =~ s#[\s]+</p>#</p>#g;
		$txt =~ s#\n\n#\n#g;

		my $head = qq(<!DOCTYPE docstd PUBLIC "-//USP//DTD DITA Doc Std//EN" "docstd_shell.dtd"[]>);
		$txt =~ s#<!DOCTYPE [^>]+>#$head#;



		if($highest_error_level != 100) {
			$errorPM->error({
				type=>'fatal',
				msg=>"check the above warnings before continuing."
			});
		}


		$txt =~ s#<image href="images/([^"]+)"/>#&img($&,$1)#ge;
		sub img {
			my $i = shift;
			my $img = shift;
			if(-e "images/$img") {
				$IMAGES{$img} = 1;
			} else {
				$IMAGES{$img} = 0;
			}

			return $i;
		}

		$txt =~ s#\n\n+#\n#g;
		return($txt);
	}
	sub step0 {
	my ($dir,$flag) = @_;
	my $direrr = 0;
	(my $windir = $dir) =~ s#[/]#\\#sg;
		if (-d $dir) {
			if ($flag eq 'onlymd') {
				#skip
			}
			else {
				#delete all files found
			}
		}
		if (!-d $dir) {
			my @created = make_path($windir);
			#######my $suc1 = mkdir($windir);
			if (-d $windir) {
				$errorPM->error({
					type=>'info',
					msg=>"created '$dir'"
				});
			}
		}
		if (!-d $dir) {
			$errorPM->error({
				type=>'fatal',
				msg=>"could NOT create $dir"
			});
			$direrr++;
		}
		if ($direrr) {
			$errorPM->error({
				type=>'fatal',
				msg=>"aborting! see above errros"
			});
                }
	}
	sub zipctl {
	my @Zip = ();
		$SETUP{'zipname'} = $SETUP{'pack'}.'/'.$SETUP{'zip'};
		push @Zip,'<zipxml>';
		push @Zip,'<zipfile name="'.$SETUP{'zipname'}.'.zip">';
		push @Zip,'<files2zip>';
		foreach my $file (sort(keys(%ZIPHASH))) {
			push @Zip, '<file name="'.$file.'" newname="'.$ZIPHASH{$file}{'zipup'}.'"/>';
		}
		push @Zip,'</files2zip>';
		push @Zip,'</zipfile>';
		push @Zip,'</zipxml>';
		my $zipx = join"\n",@Zip;
		io($SETUP{'zipctl'})->utf8->write($zipx);
		&zipup(\@Zip);
	}
	sub zipup {
	my ($zarr) = @_;
		if (scalar(@$zarr)) {
			my $zipctl = join'',@$zarr;
			$errorPM->error({
				type=>'info',
				msg=>'Packing... patience...'
			});
			$zipctlPM->zip_proc($zipctl);
		}
		else {
			$errorPM->error({
				type=>'fatal',
				msg=>"No files to zip up"
			});

		}
		my $zipbase = $SETUP{'zipname'}.'.zip';
		my $outzip = $SETUP{'pack'}.'/'.$zipbase;
		if (-e $SETUP{'zipctl'}) {
        }
	}

}
