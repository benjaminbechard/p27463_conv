#!perl

use strict;
use sigtrap;
use diagnostics;

use File::Basename;
use File::Path;
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Smart::Comments;
use utf8;
$|=1;
binmode STDOUT, ":utf8";
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $errorPM;

my $OLDSIGWARN = $SIG{__WARN__};
local $SIG{__WARN__} = sub {
	my $message = shift;
	local $| = 1;
	#&$OLDSIGWARN($message);
	my $e = log::error->new("Error Log File","programming_error.elg",$Version);
	$e->error({type=>'fatal',msg=>"$message"});
};


{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %TFOOTHASH = ();


	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'input=s', "input file", { required => 1  } ],
			[ 'output=s', "input file", { required => 1  } ],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();

	push @InputFiles,$SETUP{'opt_input'};
	&processfile(); #new 04.18.2013
	#-------------------------------------------------
	sub processfile {
		foreach my $input_file (@InputFiles) {
			($filename = $input_file) =~ s#([.])[^.]+$##s;
			$filename_ext = $2;
			#-----------Elg------------------
			my $elg = $filename;
			#$elg =~ s#^.*?[\\/]([^\\/]+)$#$1#s;
			$elg .= '.elg';
			$errorPM = log::error->new("Error Log File","$filename.elg",$Version);
			#$elg = $SETUP{'home'}.'\\'.$elg;
			if (-r $input_file) {
				my $text = io($input_file)->utf8->slurp;
				$text = &procline($text);

				#need more options here
				io($SETUP{'opt_output'})->utf8->write($text);
				$errorPM->finish(1);
			}
			else {
				$errorPM->error({
					type=>'fatal',
					msg=>"could not read $input_file"
				});
			}
		}
	}
	sub procline {
	my ($txt) = @_;
		my $dir = lc(getcwd());
		my ($tab,$unit) = $dir =~ m#active/([^/]+)/([^/]+)/work#;

		$txt =~ s#(section )(\d{1,2})([^\d])#&sections($1,$2,$3)#ge if $tab == 2;

		$txt = &proc1($txt);

		$txt =~ s#\n##g;

		while($txt =~ s#</p>(<figpara>.*?</figpara>)#$1</p>#g){;}

		$txt = figpara($txt);

		return( $txt );
	}
	sub figpara {
	my ($txt,$id) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:p)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if($tag eq 'p') {
					if($tmp1 =~ /figure-group/) {
						if($tmp1 =~ m#<xref ([^>]+)>.*?</xref>#s) {
							my $h = $1;
							my ($href) = $h =~ m#href="([^"]+)"#;
							if($tmp1 =~ s#<image/>#<image href="$href"/>#) {
								$tmp1 =~ s#<xref ([^>]+)>.*?</xref>##;
							}
							
						}
					}
					while($tmp1 =~ s#<figpara>(.*?)</figpara>##) {
						my $fp = $1;
						if($tmp1 !~ s#</title>#<brk/>$fp</title>#) {
							my $move = qq(<p inline="N">$fp</p>);
							$tmp2 .= $move;
							#$errorPM->error({
							#	type=>'fatal',
							#	msg=>"could not fix figpara"
							#});
						}
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub proc1 {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:figure-group|docstd|p|entry)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'figure-group') {
					$tmp1 =~ s#figtitle#title#g;
					if($tmp1 !~ /<image(?: [^>]+)?>/) {
						$tmp1 .= "<image/>";
					}
                }
				if($tag eq 'docstd') {
					if(defined $$atthash{'id'}) {
						if($tmp1 =~ /<xref/) {
							$tmp1 = update($tmp1,$$atthash{'id'});
						}
					}
				}
				if($tag eq 'p') {
					if($tmp1 =~ /<b>/) {
						$tmp1 =~ s#\n##g;
						if($tmp1 =~ m#<b>[\sa-zA-Z]+:#) {
							if($tmp1 =~ s#</b>$##){
								$tmp1 =~ s#<b>[\sa-zA-Z]+:#$&</b>#;
							}
						}
					} else {
						if($tmp1 =~ m#^[\sa-zA-Z0-9\.]+:#) {
							my $i = $&;
							my ($cnt) = $i =~ s#\.#$&#g;
							if($cnt < 2) {
								$tmp1 =~ s#[\sa-zA-Z0-9\.]+:#<b>$&</b>#;
							}
						}
					}
				}
				if($tag eq 'entry') {
					my ($cnt) = $tmp1 =~ s#<p>#$&#g;
					if($cnt == 1) {
						if($tmp1 =~ m#^\s*<p># and $tmp1 =~ m#</p>\s*$#) {
							$tmp1 =~ s#</?p>##g;
						}
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub update {
	my ($txt,$id) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:xref)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if($tag eq 'xref') {
					if(defined $$atthash{'href'}) {
						if($$atthash{'href'} =~ /REF\d+/) {
							$mtch = $commonPM->addattribvalue($mtch,'href',$id);
						}
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub sections {
		my ($sec,$n,$o) = @_;

		my %SECTIONS = qw(1 1_Overview.xml 2 2_Requirements_Process_and_Submissions.xml 3 3_Process_Overview_Flowchart.xml 4 4_Product_Acceptance.xml 5 5_Product_Categorization.xml
		   6 6_Initial_GMP_Quality_Systems_Facility_Audit.xml 7 7_GMP_Quality_Systems_Facility_Audit_Process.xml 8 8_Submission_of_Product_Samples_and_Documentation.xml
		   9 9_Product_Quality_Control_and_Manufacturing_(QCM)_Evaluation.xml 10 10_Testing_of_Product_Samples.xml 11 11_Classification_of_Observations.xml
		   12 12_Product_Approval_Process.xml 13 13_Use_of_the_USP_Verified_Mark.xml 14 14_Change_Notification_Process.xml 15 15_Post-Verification_Surveillance.xml
		   16 16_Mark_Usage_Suspension_Product_Recalls_and_Appeals.xml 17 17_Glossary.xml 18 18_Appendix.xml);

		if(defined $SECTIONS{$n}) {
			return qq($sec<xref href="$SECTIONS{$n}">$n</xref>$o);
		}

		return qq($sec$n$o);


	}

}
