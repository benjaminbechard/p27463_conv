 Normal    {</topic?><topic?><title>}!
 clf =exclusive= =font-weight,Bold= =font-slant,Ital=  {<b><i>}!
 clf =exclusive= =vertical-offset,-s= =font-slant,Ital=  {<i><sub>}!
 clf =exclusive= =font-slant,Ital=   {<i>}!
 clf =exclusive= =font-weight,Bold=  {<b>}!
 clf =exclusive= =clftype,Bold=   {<b>}!
 clf =exclusive= =vertical-offset,+s=   {<sup>}!
 clf =exclusive= =vertical-offset,-s=   {<sub>}!
 clf =exclusive= =font-slant,Ital= =vertical-offset,+s=   {<sup><i>}!
 clf =exclusive= =lowercase-display,SmallCaps=   {<smallcaps>}!
 clf =exclusive= =lowercase-display,FullCaps=   {<dclfullcaps>}!
 clf =exclusive= =score-location,under= =score-type,single=  {<u>}!
 clf =exclusive= =score-location,under= =score-type,single= =font-slant,Ital= {<u><i>}!
 clf =exclusive= =score-location,under= =score-type,single= =font-slant,Ital= =font-weight,Bold= {<u><i><b>}!
 colspec	{dumptag}
 dcl-bultext1  {<list1 dcl_flag="ul"?></listitem1?><listitem1?><lipara>}!
 dcl-bultext2  {<list2 dcl_flag="ul"?></listitem2?><listitem2?><lipara>}!
 dcl-donothing   {}!
 dcl-empty  {empty}
 dcl-emptyspace {empty<dummy> }
 dcl-figgrp      {<para><fig-group><title>}!
 dcl-fn   {<para><fn>}!
 dcl-list2ol  {<list2 dcl_flag="ol"?></listitem2?><listitem2?><lipara>}!
 dcl-list3ol  {<list3 dcl_flag="ol"?></listitem3?><listitem3?><lipara>}!
 dcl-numtext1 {<list1 dcl_flag="ol"?></listitem1?><listitem1?><lipara>}!
 dcl-numtext2 {<list2 dcl_flag="ol"?></listitem2?><listitem2?><lipara>}!
 dcl-para  {<para inline="N">}!
 dcl-parainline  {<para inline="Y">}!
 dcl-ref   {<tableref>}!
 dcl-section1    {</section1?><section1?><title>}!
 dcl-section2    {</section2?><section2?><title>}!
 dcl-semiital    {</section1?><section1?><title><i>}!
 dcl-skip  {skiptext}
 dcl-title {</septopic?><septopic?><title>}!
 entry {stacktag}
 entrypara {stacktag}
 hyperlink =rid= {<xref href="[[rid]]" format="html" scope="external">}!
 row {stacktag}
 styinfo   {skiptext}
 synonym   dcl-para Body`no`indent Body Body`Hang Body`hang NormalParagraphStyle Table`text Table`col`heads Numlist`bold`lead Body`Text No`paragraph`style
 synonym   dcl-parainline dclinline
 synonym   dcl-donothing anchor
 synonym   dcl-section1 Subhead dclsec1 Subhead`B Heading`1 heading`1 
 synonym   dcl-semiital Subhead`semi`ital
 synonym   dcl-section2 dclsec2 Heading`2 heading`2
 synonym   dcl-empty pgbrk cr
 synonym   dcl-emptyspace tab
 synonym   dcl-skip sect z_header
 synonym   dcl-figgrp Figure`name
 synonym   dcl-ref Table`References
 synonym   dcl-bultext1 bullet`text
 synonym   dcl-numtext1 Numbered`text1 numbered`text1
 synonym   dcl-bultext2 bullet`text2
 synonym   dcl-numtext2 Numbered`text numbered`text
 synonym   dcl-list2ol List
 synonym   dcl-list3ol List`2
 synonym   dcl-title Title
 synonym   dcl-fn footnote`text
 table {stacktag}!
 tbody {stacktag}
 tfoot {stacktag}
 tgroup {</title?>stacktag}
 thead {stacktag}
 title   {<title>}!
