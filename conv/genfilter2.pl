#!perl

use strict;
use sigtrap;
use diagnostics;

use File::Basename;
use File::Path;
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Smart::Comments;
use utf8;

$|=1;
binmode STDOUT, ":utf8";
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $errorPM;
my $UNIQUEIDREF=1;
my $OLDSIGWARN = $SIG{__WARN__};
local $SIG{__WARN__} = sub {
	my $message = shift;
	local $| = 1;
	#&$OLDSIGWARN($message);
	my $e = log::error->new("Error Log File","programming_error.elg",$Version);
	$e->error({type=>'fatal',msg=>"$message"});
};


{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %FOOTNOTE;
my %UNIQUEID;
my $marsid="";
	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'input=s', "input file", { required => 1  } ],
			[ 'output=s', "input file", { required => 1  } ],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();

	push @InputFiles,$SETUP{'opt_input'};
	&processfile(); #new 04.18.2013
	#-------------------------------------------------
	sub processfile {
		foreach my $input_file (@InputFiles) {
			($filename = $input_file) =~ s#([.])[^.]+$##s;
			$filename_ext = $2;
			#-----------Elg------------------
			my $elg = $filename;
			#$elg =~ s#^.*?[\\/]([^\\/]+)$#$1#s;
			$elg .= '.elg';
			$errorPM = log::error->new("Error Log File","$filename.elg",$Version);
			#$elg = $SETUP{'home'}.'\\'.$elg;
			if (-r $input_file) {
				my $text = io($input_file)->utf8->slurp;
				$text = &procline($text);

				#need more options here
				io($SETUP{'opt_output'})->utf8->write($text);
				$errorPM->finish(1);
			}
			else {
				$errorPM->error({
					type=>'fatal',
					msg=>"could not read $input_file"
				});
			}
		}
	}
	sub procline {
	my ($txt) = @_;


		my $dir = lc(getcwd());
		my ($tab,$unit) = $dir =~ m#active/([^/]+)/([^/]+)/work#;
		($marsid) = $txt =~ m#marsid="([^"]+)"#;

		if($tab =~ /[12456]/) {
			$txt = &proc1($txt,$tab);
		}

		$txt = &sup2fn($txt,$tab);

		foreach my $k (keys %FOOTNOTE) {
			$errorPM->error({
				type=>'warning',
				msg=>"footnote $k not used in document"
			}) if $FOOTNOTE{$k}{used} == 0;
		}

		#$txt = &li($txt);
		#$txt = &li($txt) if $tab =~ /^[1]$/;

		return( $txt );
	}
	sub li {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:stdbody)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'stdbody') {
					my ($pcnt) = $tmp1 =~ s#</p>#$&#g;
					if($pcnt > 1) {
						my $t1 = $tmp1;
						my $list = 0;
						while($t1 =~ s#<p(?: [^>]+)?>(.*?)</p>##) {
							my $cont = $1;
							if($cont =~ m#^[0-9]+[.\)] #) {
								$list++;
							}
						}
						if($list > 0) {
							my $c = 0;
							while($tmp1 =~ s#<p(?: [^>]+)?>([0-9]+[.\)] )(.*?)</p>#<li><dclp>$2</dclp></li></dclol$c>#s) {
								$c++;
							}
							$c--;
							$tmp1 =~ s#<li>#<ol><li>#;
							$tmp1 =~ s#</dclol$c>#</ol>#;
							$tmp1 =~ s#</dclol\d+>##g;
							$tmp1 =~ s#<(/?)dclp>#<$1p>#g;
						}
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub sup2fn {
	my ($txt,$tab) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:sup)";
	my $skipflag = 0;
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
#		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
#			$lft = $`;
#			$mtch = $&;
#			$txt = $';
#			my $tag = $1;
#			my $atthash = $commonPM->getattribs($mtch);
#			if ($txt =~ m#</$tag>#si) {
#				my $tmp1 = $`;
#				my $tmp2 = $&;
#				my $tmp3 = $';
#				#print $progress--," " x 6,"\r";
#				if ($tag eq 'sup') {
#					$tmp1 =~ s#\n*##g;
#					if($tmp1 =~ /^[0-9\-,\s&#;]+$/) {
#						my @fns;
#						$tmp1 =~ s/(\d+)(?:-|&#x02013;)(\d+)/join(',',$1..$2)/eg;
#						@fns = map {qq(<sup>$_</sup>)} (split /,/, $tmp1);
#						my $new = join ",", @fns;
#						#foreach (@fns) {
#						#	$new .= qq(<sup>$_</sup>);
#						#}
#						$mtch = $new;
#						$tmp1 = $tmp2 = "";
#					}
#                }
#				$txt = $tmp1.$tmp2.$tmp3;
#			}
#			$newtxt .= $lft. $mtch;
#        }
#		$txt = $newtxt . $txt;

		($lft,$mtch,$newtxt) = ("","","");
		$tagxpr = "(?:sup)";
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'sup') {
					if(not defined $$atthash{'noxref'}) {
						$tmp1 =~ s#\n*##g;
						my $nexttonumber=0;

						my $isfnxref = 1;
						my $b4 = substr $lft, -10;
						if($b4 =~ /[0-9]+$/) {
							$nexttonumber = 1;
						}
						my $x = $tmp1;
						$x =~ s#[\(\)]##g;
						my @fns = split /(-|&#x02013;|,)/, $x;
						foreach (@fns) {
							if(not defined $FOOTNOTE{$_}) {
								if($_ !~ /(-|&#x02013;|,)/) {
									$isfnxref = 0;
								}
							}
						}
						my $new = "";
						if($isfnxref) {
							foreach (@fns) {
								if(defined $FOOTNOTE{$_} and (($nexttonumber and $_ !~ /^[0-9]+$/) or !$nexttonumber)) {
									$new .= qq(<xref href="$FOOTNOTE{$_}{id}" format="dita">$_</xref>);
									$FOOTNOTE{$_}{used} = 1;
								} else {
									$new .= $_;
								}
							}
							$tmp1 = $new;
						}
					} else {
						$mtch = $commonPM->delattrib($mtch,'noxref');
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;

	return( $txt );
	}
    sub num2range {
        local $_ = join ',' => sort { $a <=> $b } @_;
        s/(?<!\d)(\d+)(?:,((??{$++1})))+(?!\d)/$1-$+/g;
        return $_;
    }
	sub proc1 {
	my ($txt,$tab) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:title|docstd)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		$txt = $commonPM->nesting($txt,"(?:docstd)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'title') {
					$tmp1 =~ s#</?b>##g;
                }
				if($tag =~ /^docstd_[0-9]+$/) { #and $tab =~ /[1]/
					if($tmp1 !~ /<docstd_/) {
						my $isref = 0;
						my ($firsttitle) = $tmp1 =~ m#<title>(.*?)</title>#s;
					#if($tmp1 =~ /(<tableref>)/i or $firsttitle =~ /(references)/i) {
						($tmp1, $isref) = references($tmp1,"m${marsid}REF$UNIQUEIDREF");
						if($isref) {
							$mtch = $commonPM->addattribvalue($mtch,'id',"m${marsid}REF$UNIQUEIDREF");
							$UNIQUEIDREF++;
						}
					#}
					}

				}
				if($tag =~ /^docstd_1$/) {
					if($tmp1 =~ m~<sup>\s*((?:(?:CR\d+)(&#x0*2013;|-| |,|[*])?){1,})\s*<\/sup>~i) {
						$errorPM->error({
							type=>'fatal',
							msg=>"marsid not found"
						}) if not defined $$atthash{'marsid'};

						my $mrid = $$atthash{'marsid'};
						my $id = "m${mrid}CR1";
						my $cr = qq(<docstd id="$id"><title>Certified References</title><stdbody><p>placeholder</p></stdbody></docstd>);
						$tmp1 .= $cr;

						$tmp1 =~ s~<sup>\s*((?:(?:CR\d+)(&#x0*2013;|-| |,|[*])?){1,})\s*<\/sup>~<xref href="#$id" format="dita">$1</xref>~ig;

					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
		$txt =~ s#<(/?)docstd_\d+#<$1docstd#g;
	return( $txt );
	}
	sub references {
	my ($txt,$id) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:stdbody|tableref)";
	my %hash = ();
	my $isref = 0;
	my $tableid = 0;
	my @TABLES;
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'stdbody') {
					while($tmp1 =~ s#<table(?: [^>]+)?>.*?</table>#DCLTABLE$tableid#s) {
						push @TABLES,$&;
						$tableid++;
					}
					($tmp1,$isref) = fnentries($tmp1,$id);
					$tmp1 .= "</ol>" if $tmp1 =~ m#<ol>#;
					my $i = 0;
					foreach my $table (@TABLES) {
						$tmp1 =~ s#DCLTABLE$i#$table#;
						$i++;
					}
					if($tmp1 =~ /DCLTABLE\d+/) {
						$errorPM->error({
							type=>'fatal',
							msg=>"failed to replace all DCLTABLEid"
						});
					}
                }
				if($tag eq 'tableref') {
					my $fn;
					if($tmp1 =~ s#^\s*([*]+|\([a-zA-Z0-9]+\)|[a-zA-Z]+\s\-\s)\s*#$&#) {
						$fn = $&;
					}
					if($fn) {
						$fn =~ s#[-\(\)\s]##g;
						my $callout = $fn;
						my $id = $fn;
						$id =~ s#[*]+#uniqueid($&)#e;
						sub uniqueid {
							my $r = 1001;
							while(defined $UNIQUEID{$r}) {
								$r++;
							}
							$UNIQUEID{$r}=1;
							return $r;
						}
						$mtch = qq(<p><fn id="fn$id" callout="$callout">);
						$tmp2 = "</fn></p>";
					} else {
						$mtch = "<p>";
						$tmp2 = "</p>";
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt, $isref );
	}
	sub fnentries {
	my ($txt,$refid) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:p|tableref)";
	my %hash = ();
	my $first = 0;
	my $hasref = 0;
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^$tagxpr$/) {
					if($tmp1 !~ /^\s*&#x(0*2022|0*201C);/) {
						my $mrkr = "";
						if($tmp1 =~ s~(^\s*(?:<fn>)?([0-9]+[.]\s|\([a-zA-Z]\)\s+|[*]+|&#x[^;]+;))~$&~) {
							$mrkr = $2;
						}
						if($tmp1 =~ s~^(\s*)(?:<sup>\s*)([0-9]+[.]?\s*|\([a-zA-Z]\)\s+|[*]+|&#x[^;]+;)(\s*</sup>)~$1<sup noxref="1">$2$3~) {
							$mrkr = $2;
						}
						$mrkr = "" if $mrkr =~ m~&#x(0*A7|0*201C);~;
					if($mrkr ne "") {
						$hasref=1;
						$mrkr =~ s#[.*\s\(\)]+##g;
						my $id = "fn$mrkr";
						if(defined $FOOTNOTE{$id}) {
							$errorPM->error({
								type=>'fatal',
								msg=>"footnote found with duplicate marker [[[$tmp1]]]"
							});
						}
						$FOOTNOTE{$mrkr}{id} = $refid;
						$FOOTNOTE{$mrkr}{used} = 0;
						#$mtch = $commonPM->addattribvalue($mtch,'callout',$mrkr);
						$mtch = "<li><p>";
						if(!$first) {
							$first = 1;
							$mtch = "<ol><li><p>";
						}
						$tmp2 = "</p></li>";
						$mtch = $commonPM->delattrib($mtch,'inline');
					}
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt, $hasref );
	}
}
