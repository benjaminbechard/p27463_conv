#!perl

use strict;
use sigtrap;
use diagnostics;
use Spreadsheet::ParseXLSX;
use File::Basename;
use File::Path;
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Smart::Comments;
use utf8;
$|=1;
binmode STDOUT, ":utf8";
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $errorPM;

my $OLDSIGWARN = $SIG{__WARN__};
local $SIG{__WARN__} = sub {
	my $message = shift;
	local $| = 1;
	#&$OLDSIGWARN($message);
	my $e = log::error->new("Error Log File","programming_error.elg",$Version);
	$e->error({type=>'fatal',msg=>"$message"});
};


{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %TFOOTHASH = ();


	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'input=s', "input file", { required => 1  } ],
			[ 'output=s', "input file", { required => 1  } ],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();

	push @InputFiles,$SETUP{'opt_input'};
	&processfile(); #new 04.18.2013
	#-------------------------------------------------
	sub processfile {
		foreach my $input_file (@InputFiles) {
			($filename = $input_file) =~ s#([.])[^.]+$##s;
			$filename_ext = $2;
			#-----------Elg------------------
			my $elg = $filename;
			#$elg =~ s#^.*?[\\/]([^\\/]+)$#$1#s;
			$elg .= '.elg';
			$errorPM = log::error->new("Error Log File","$filename.elg",$Version);
			#$elg = $SETUP{'home'}.'\\'.$elg;
			if (-r $input_file) {
				my $text = io($input_file)->utf8->slurp;
				$text = &procline($text);

				#need more options here
				io($SETUP{'opt_output'})->utf8->write($text);
				$errorPM->finish(1);
			}
			else {
				$errorPM->error({
					type=>'fatal',
					msg=>"could not read $input_file"
				});
			}
		}
	}
	sub procline {
	my ($txt) = @_;

		$txt =~ s#<colspec( [^>]+)?>#$&</colspec>#g;
		$txt =~ s#entrypara#para#g;

		my $head  = qq(<?xml version="1.0" encoding="UTF-8"?>\n);
		   $head .= qq(<!DOCTYPE docstd SYSTEM "K:\\P27463\\DTD\\DocTypes\\dita-oasis\\1.2\\usp\\dtd\\docstd_shell.dtd">\n);
		$txt = $head.$txt;


		my ($docclass,$marsid,$tab);
		($txt,$docclass,$marsid,$tab) = titlesAndMarsid($txt);

		$txt = &mapping($txt);

		$txt = &proc1($txt,$docclass,$marsid,$tab);
		$txt = &stdbody($txt);

		while($txt =~ m#<(sup|sub|b|i)>[ ]+</\1>#) {
			$txt = &spaces($txt);
			$txt =~ s#<(sup|sub|b|i)></\1>##g;
		}

		return( $txt );
	}
	sub spaces {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:sup|sub|i|b)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^$tagxpr$/) {
					if($tmp1 =~ s#^[ ]+##g) {
						$mtch = " ".$mtch;
					}
					if($tmp1 =~ s#[ ]+$##g) {
						$tmp2 .= " ";
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub titlesAndMarsid {
		my ($txt) = @_;
		my ($filename,$marsid,$docclass);
		my @doc = qw(null dsc-DSadmissions dsc-DSverification dsc-Regulatory dsc-DietaryIntake dsc-Guidance dsc-Illustrations);
		my $dir = lc(getcwd());
		my ($tab,$unit) = $dir =~ m#active/([^/]+)/([^/]+)/work#;
		if(!$tab or !$unit) {
			$errorPM->error({
				type=>'fatal',
				msg=>"could not determin the unit from the current path  [[[asdf]]]"
			});
		}

		my $excel = 'K:/P27463/docs/DSC InDesign Conversion_Titles and M numbers.xlsx';
		my $xlsxparser = Spreadsheet::ParseXLSX->new;

		my $xlsxworkbook = $xlsxparser->parse($excel);
		my $sheet = $xlsxworkbook->{Worksheet}[($tab-1)];
		foreach my $row ($sheet -> {MinRow} .. $sheet -> {MaxRow}) {
			my $foldermap = $sheet->get_cell( $row, 3 );
			next unless $foldermap;
			next if lc($foldermap->value()) ne $unit;
			my $filetitle = $sheet->get_cell( $row, 0 );
			next unless $filetitle;

			my $uspid = $sheet->get_cell( $row, 2 );
			next unless $uspid;


			$marsid = $uspid->value();
			$filename = $filetitle->value();
		}
		undef $sheet;

		undef $xlsxworkbook;
		undef $xlsxparser;

		if(!$marsid or !$filename) {
			$errorPM->error({
				type=>'fatal',
				msg=>"could not find a marsid or a filename from the spreadsheet  [[[$dir]]]"
			});
		}



		$docclass = $doc[$tab];

		my $newname = qq(${marsid}_${filename});
		$newname =~ s#(^\s+|\s+$|[:])##g;
		io("rename.txt")->write($newname);
		return ($txt,$docclass,$marsid,$tab);
	}
	sub stdbody {
	my ($txt,$docclass,$marsid) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:docstd|septopic)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		$txt = $commonPM->nesting($txt,"(?:docstd|septopic)");
		my ($doccnt) = $txt =~ s#<docstd_#$&#g;
		if($doccnt > 1) {
			if($txt !~ /<docstd_2/) {
				$txt =~ s#<(/?)docstd_\d+#<$1docstd#g;
				$txt =~ s#<docstd( [^>]+)?>#$&<docstd>#;
				$txt .= "</docstd>";
				$txt = $commonPM->nesting($txt,"(?:docstd|septopic)");
			}
		}
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /docstd_[1-4]|septopic_[1-4]/) {
					$tmp1 =~ s#\n##g;
					if($tmp1 =~ s#^(<title(?: [^>]+)?>.*?</title>)(.*)$##) {
						my $title = $1;
						my $body = $2;
						my $subdocstd = "";
						if($body =~ s#^(.*?)(<docstd_\d+(?: [^>]+)?>.*)$#$1#) {
							$subdocstd = $2;
						}
						if($body !~ m#<docstd# and $body !~ /^\s*$/) {
							$body = qq(<stdbody>$body</stdbody>);
						}
						$body .= $subdocstd;
						if(defined $$atthash{dclinline}) {
							$body =~ s#<p>#<p inline="Y">#g;
							$mtch = $commonPM->delattrib($mtch,'dclinline');
						}
						$tmp1 = qq($title$body);
					}

                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
		$txt =~ s#<(/?)(docstd|septopic)_\d+#<$1$2#g;
	return( $txt );
	}
	sub mapping {
	my ($txt,$docclass,$marsid) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para|section[1-4]b?|dclbody|dclsec[12]|dclnoinline)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {
					$tmp2 = "</p>";
					$mtch =~ s#<para#<p#;
                }
				if($tag =~ /section[1-4]b?/) {
					$tmp2 = "</docstd>";
					$mtch =~ s#<section[1-4]b?#<docstd#;
				}
				if($tag eq 'dclbody') {
					$mtch =~ s#<dclbody((?: [^>])?)>#<docstd$1><title/><stdbody><p>#;
					$tmp2 = qq(</p></stdbody></docstd>);
				}
				if($tag =~ /dclsec1/) {
					$tmp2 = "</docstd>";
					$mtch =~ s#<dclsec1#<docstd dclinline="Y"#;
				}
				if($tag =~ /dclsec2/) {
					$tmp2 = "</docstd>";
					$mtch =~ s#<dclsec2#<docstd dclinline="Y"#;
				}
				if($tag eq 'dclnoinline') {
					$tmp2 = "</p>";
					$mtch =~ s#<dclnoinline#<p inline="N"#;
				}


				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub proc1 {
	my ($txt,$docclass,$marsid,$tab) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:(?:sep)?topic)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
	if($txt !~ /<topic/ and $tab == 6) {
		$txt = $commonPM->nesting($txt,"(?:docstd)");
		$txt =~ s#<(/?)docstd_1#<$1topic#g;
		$txt =~ s#<(/?)docstd_\d+#<$1docstd#;
	}
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^$tagxpr$/) {
					my $id = lc($filename);
					$id =~ s#\s+#_#g;
					$id =~ s#[-\(\)']##g;
					$mtch = $commonPM->addattribvalue($mtch,'docclass',$docclass);
					$marsid =~ s#^[^\d]+##;
					$mtch = $commonPM->addattribvalue($mtch,'marsid',$marsid);
					if($tag eq 'topic') {
						$tmp2 = "</docstd>";
						$mtch =~ s#<topic#<docstd#;
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
}
