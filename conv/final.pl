#!perl

use strict;
use sigtrap;
use diagnostics;

use File::Basename;
use File::Path;
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Smart::Comments;
use utf8;
$|=1;
binmode STDOUT, ":utf8";
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $errorPM;

my $OLDSIGWARN = $SIG{__WARN__};
local $SIG{__WARN__} = sub {
	my $message = shift;
	local $| = 1;
	#&$OLDSIGWARN($message);
	my $e = log::error->new("Error Log File","programming_error.elg",$Version);
	$e->error({type=>'fatal',msg=>"$message"});
};


{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %TFOOTHASH = ();
my $FIGID=0;
my $DOCTITLE = "";
	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'input=s', "input file", { required => 1  } ],
			[ 'output=s', "input file", { required => 1  } ],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();

	push @InputFiles,$SETUP{'opt_input'};
	&processfile(); #new 04.18.2013
	#-------------------------------------------------
	sub processfile {
		foreach my $input_file (@InputFiles) {
			($filename = $input_file) =~ s#([.])[^.]+$##s;
			$filename_ext = $2;
			#-----------Elg------------------
			my $elg = $filename;
			#$elg =~ s#^.*?[\\/]([^\\/]+)$#$1#s;
			$elg .= '.elg';
			$errorPM = log::error->new("Error Log File","$filename.elg",$Version);
			#$elg = $SETUP{'home'}.'\\'.$elg;
			if (-r $input_file) {
				my $text = io($input_file)->utf8->slurp;
				$text = &procline($text);

				#need more options here
				my $dir = lc(getcwd());
				my ($tab,$unit) = $dir =~ m#active/([^/]+)/([^/]+)/work#;
				if($tab !~ /2/) {
					my $newname = io('rename.txt')->slurp;
					$newname =~ s#\n##g;
					io("$newname.f95")->utf8->write($text);
				} else {
					outdoc($text);
					unlink "rename.txt";
				}
				$errorPM->finish(1);
			}
			else {
				$errorPM->error({
					type=>'fatal',
					msg=>"could not read $input_file"
				});
			}
		}
	}
	sub procline {
	my ($txt) = @_;


		($DOCTITLE) = $txt =~ m#<title>(.*?)</title>#;
		$txt =~ s#\n##g;
		$txt =~ s#<stdbody>\n*</stdbody>#<stdbody><p/></stdbody>#g;

		while($txt =~ s#</(i|b|u)>((?:\s|\n)*)<\1>#$2#gs){;}

		$txt = dclfullcaps($txt);



		$txt = &proc2($txt);


		$txt =~ s#(<[^/])#\n$1#g;
		$txt =~ s#^\n+##;
		$txt =~ s#  # #g;

		$txt = $commonPM->full2empty($txt);

		return( $txt );
	}

	sub dclfullcaps {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:dclfullcaps|figure-group|image)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		$txt = $commonPM->empty2full($txt);
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'dclfullcaps') {
					my $temp = $tmp1;
					$temp =~ s#(&[^;]+;|<sup>.*?</sup>|<sub>.*?</sub>|<[^>]+>)##g;
					if($temp =~ /[a-z]/) {
						$errorPM->error({
							type=>'fatal',
							msg=>"found lowercase letters in [[$tmp1]]"
						});
					}
					$mtch = $tmp2 = "";
                }
				if($tag eq 'figure-group'){
					$mtch = $commonPM->addattribvalue($mtch,'outputclass','illustration');
					$FIGID++;
					$mtch = $commonPM->addattribvalue($mtch,'id',"fig".$FIGID);
					$tmp1 =~ s#<image>#<image href="${DOCTITLE}_f$FIGID">#;
				}
				if($tag eq 'image') {
					#$mtch = $commonPM->addattribvalue($mtch,'href',"");
					$mtch = $commonPM->addattribvalue($mtch,'placement',"break");
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
		#$txt = $commonPM->full2empty($txt);
	return( $txt );
	}
	sub proc2 {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:p|title|stdbody|docstd)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^$tagxpr$/) {
					$tmp1 =~ s#(^[ ]|[ ]$)##g;
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub outdoc {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:septopic)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^$tagxpr$/) {
					my ($title) = $$atthash{'septitle'};
					$mtch = $commonPM->delattrib($mtch,'septitle');
					my $head  = qq(<?xml version="1.0" encoding="UTF-8"?>\n);
					   $head .= qq(<!DOCTYPE docstd SYSTEM "K:\\P27463\\DTD\\DocTypes\\dita-oasis\\1.2\\usp\\dtd\\docstd_shell.dtd">\n);

					$tmp2 = "</docstd>";
					$mtch =~ s#<septopic#<docstd#;

					io("${title}.xml")->utf8->write("$head$mtch$tmp1$tmp2");
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
}
