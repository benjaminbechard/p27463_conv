 Normal    {</topic?><topic?><title>}!
 ^table {</table?>}
 clf =exclusive= =font-weight,Bold= =font-slant,Ital=  {<b><i>}!
 clf =exclusive= =vertical-offset,-s= =font-slant,Ital=  {<i><sub>}!
 clf =exclusive= =font-slant,Ital=   {<i>}!
 clf =exclusive= =font-weight,Bold=   {<b>}!
 clf =exclusive= =clftype,Bold=   {<b>}!
 clf =exclusive= =vertical-offset,+s=   {<sup>}!
 clf =exclusive= =vertical-offset,-s=   {<sub>}!
 clf =exclusive= =font-slant,Ital= =vertical-offset,+s=   {<sup><i>}!
 clf =exclusive= =font-weight,Bold= =vertical-offset,+s=   {<sup><b>}!
 clf =exclusive= =score-location,under= =score-type,single= =vertical-offset,+s=   {<sup><u>}!
 clf =exclusive= =lowercase-display,SmallCaps=   {<smallcaps>}!
 clf =exclusive= =lowercase-display,FullCaps=   {<dclfullcaps>}!
 clf =exclusive= =lowercase-display,FullCaps= =font-slant,Ital= =font-weight,Bold= {<dclfullcaps><i><b>}!
 clf =exclusive= =lowercase-display,FullCaps= =font-slant,Ital= {<dclfullcaps><i>}!
 clf =exclusive= =score-location,under= =score-type,single=  {<u>}!
 clf =exclusive= =score-location,under= =score-type,single= =font-slant,Ital= {<u><i>}!
 clf =exclusive= =score-location,under= =score-type,single= =font-slant,Ital= =font-weight,Bold= {<u><i><b>}!
 clf =exclusive= =vertical-offset,+s= =font-slant,Ital= =font-weight,Bold= {<sup><i><b>}!
 clf =exclusive= =lowercase-display,SmallCaps= =font-weight,Bold=   {<b><smallcaps>}!
 clf =exclusive= =lowercase-display,SmallCaps= =font-slant,Ital=   {<i><smallcaps>}!
 clf =exclusive= =font-weight,Bold= =vertical-offset,-s= =font-slant,Ital=  {<sub><i><b>}!
 clf =exclusive= =font-weight,Bold= =lowercase-display,SmallCaps= =font-slant,Ital=  {<i><b><smallcaps>}!
 clf =exclusive= =font-weight,Bold= =vertical-offset,-s=   {<sub><i>}!
 colspec	{dumptag}
 dcl-bultext  {<dclbullist?><bulitem>}!
 dcl-donothing   {}!
 dcl-empty  {empty}
 dcl-emptyspace {empty<dummy> }
 dcl-figgrp      {<para><figure-group><figtitle><b>}!
 dcl-figpara		{<figpara>}!
 dcl-fn   {<para><fn>}!
 dcl-numtext {<dclnumlist?><numitem>}!
 dcl-para  {<para inline="N">}!
 dcl-parainline  {<para inline="Y">}!
 dcl-ref   {<tableref>}!
 dcl-section1    {</section1?><section1?><title>}!
 dcl-section1b    {</section1b?><section1b?><title>}!
 dcl-section2    {</section2?><section2?><title>}!
 dcl-section3    {</section3?><section3?><title>}!
 dcl-section4    {</section4?><section4?><title>}!
 dcl-section5    {</section5?><section5?><title>}!
 dcl-semiital    {</section1?><section1?><title><i>}!
 dcl-skip  {skiptext}
 dcl-title {</section2?><section2?><title>}!
 entry {stacktag}
 entrypara {stacktag}
 hyperlink =rid= {<xref href="[[rid]]" format="html" scope="external">}!
 list1ol  {<list1 dcl_flag="ol"?></listitem1?><listitem1?><lipara>}!
 list1ul  {<list1 dcl_flag="ul"?></listitem1?><listitem1?><lipara>}!
 list2ol  {<list2 dcl_flag="ol"?></listitem2?><listitem2?><lipara>}!
 list2ul  {<list2 dcl_flag="ul"?></listitem2?><listitem2?><lipara>}!
 list3ol  {<list3 dcl_flag="ol"?></listitem3?><listitem3?><lipara>}!
 list3ul  {<list3 dcl_flag="ul"?></listitem3?><listitem3?><lipara>}!
 list4ol  {<list4 dcl_flag="ol"?></listitem4?><listitem4?><lipara>}!
 list5ol  {<list5 dcl_flag="ol"?></listitem5?><listitem5?><lipara>}!
 list6ol  {<list6 dcl_flag="ol"?></listitem6?><listitem6?><lipara>}!
 row {stacktag}
 styinfo   {skiptext}
 synonym   dcl-para Body`no`indent Body Body`Hang Body`hang NormalParagraphStyle Table`text Table`col`heads Numlist`bold`lead Body`Text No`paragraph`style Parameters Parameters`last Track`Assignment`List
 synonym   dcl-parainline dclinline
 synonym   dcl-donothing anchor
 synonym   dcl-section1 Subhead Illustrations-page`head
 synonym   dcl-section1b  dclsec1 Head`A BOTANICAL-caps
 synonym   dcl-semiital Subhead`semi`ital Subhead`ital Head`A`sub
 synonym   dcl-section2 dclsec2 Heading`1 heading`1 Heading`2 heading`2 Subhead`B Subhead`2 Head`B
 synonym   dcl-section3 dclsec3 Head`C a`Macroscopic
 synonym   dcl-section4 Head`D b`Head`D
 synonym   dcl-section5 Head`E
 synonym   dcl-empty pgbrk cr mark dcldrop Figure`Labels
 synonym   dcl-emptyspace tab
 synonym   dcl-skip sect z_header footnotes
 synonym   dcl-figgrp Figure`name Figure`space
 synonym   dcl-figpara Figure`identifiers Figure`Label Figure`text`semibold Figure`text
 synonym   dcl-ref Table`References
 synonym   dcl-numtext Numbered`text
 synonym   dcl-bultext bullet`text
 synonym   dcl-title Title
 synonym   dcl-fn footnote`text footnote
 table {stacktag}
 tbody {stacktag}
 tfoot {stacktag}
 tgroup {</title?>stacktag}
 thead {stacktag}
 title   {<title>}!
 xref {stacktag}
