#!perl

use strict;
use sigtrap;
use diagnostics;

use File::Basename;
use File::Path;
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Smart::Comments;
use utf8;
$|=1;
binmode STDOUT, ":utf8";
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $errorPM;

my $OLDSIGWARN = $SIG{__WARN__};
local $SIG{__WARN__} = sub {
	my $message = shift;
	local $| = 1;
	#&$OLDSIGWARN($message);
	my $e = log::error->new("Error Log File","programming_error.elg",$Version);
	$e->error({type=>'fatal',msg=>"$message"});
};


{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %TFOOTHASH = ();


	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'input=s', "input file", { required => 1  } ],
			[ 'output=s', "input file", { required => 1  } ],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();

	push @InputFiles,$SETUP{'opt_input'};
	&processfile(); #new 04.18.2013
	#-------------------------------------------------
	sub processfile {
		foreach my $input_file (@InputFiles) {
			($filename = $input_file) =~ s#([.])[^.]+$##s;
			$filename_ext = $2;
			#-----------Elg------------------
			my $elg = $filename;
			#$elg =~ s#^.*?[\\/]([^\\/]+)$#$1#s;
			$elg .= '.elg';
			$errorPM = log::error->new("Error Log File","$filename.elg",$Version);
			#$elg = $SETUP{'home'}.'\\'.$elg;
			if (-r $input_file) {
				my $text = io($input_file)->utf8->slurp;
				$text = &procline($text);

				#need more options here
				io($SETUP{'opt_output'})->utf8->write($text);
				$errorPM->finish(1);
			}
			else {
				$errorPM->error({
					type=>'fatal',
					msg=>"could not read $input_file"
				});
			}
		}
	}
	sub procline {
	my ($txt) = @_;

		my $dir = lc(getcwd());
		my ($tab,$unit) = $dir =~ m#active/([^/]+)/([^/]+)/work#;

		$txt =~ s#</dclnumlist>\n*<dclnumlist>##g;
		$txt =~ s#</dclbullist>\n*<dclbullist>##g;

		$txt = listitem($txt);
		$txt = dclnextpara($txt);




		$txt = &proc1($txt);

		$txt = &proc2($txt,$tab);


		return( $txt );
	}
	sub proc2 {
	my ($txt,$tab) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:u|li|septopic)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		$txt = $commonPM->nesting($txt,"(?:li)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^(u)$/) {
					$tmp1 =~ s#\n##g;
					if($tmp1 =~ m~^(((ht|f)tp(s?))\://)?(www.|[a-zA-Z].)[a-zA-Z0-9\-\.]+\.(com|edu|gov|mil|net|org|biz|info|name|museum|us|ca|uk|[a-z]+)(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\;\?\'\\\+&%\$#\=\~_\-]+))*$~i) {
						$mtch = $tmp2 = "";
						my $format = "html";
						if($tmp1 =~ m#[.]pdf$#) {
							$format = "pdf";
						}
						$tmp1 = qq(<xref href="$tmp1" format="$format" scope="external">$tmp1</xref>);
					}
                }
				if($tag =~ /^li/) {
					if($tab =~ /[145]/) {
						$tmp1 =~ s~^(\s*<p>)\s*((?:<u>|<b>)?)([a-z0-9]+[.\)]|&#x02022;)(\2)?\s+~$1~;
					}
				}
				if($tag eq 'septopic') {
					my $imgindex = 1;
					my ($title) = $tmp1 =~ m#^s*<title>(.*?)</title>#s;
					$title = cleantitle($title);
					$mtch = $commonPM->addattribvalue($mtch,'septitle',$title);
					if($tmp1 =~ /\[\[img\]\]/) {
						my $i = sprintf("%03d",$imgindex);
						while($tmp1 =~ s#\[\[img\]\]#<figure-group><image href="${title}_fig$i.jpg"/></figure-group>#) {
							$imgindex++;
							$i = sprintf("%03d",$imgindex);
						}
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
		$txt =~ s#<(/?)(li)_\d+#<$1$2#g;
	return( $txt );
	}
	sub cleantitle {
		my ($txt) = @_;

		$txt =~ s#[\s\.,]#_#g;
		$txt =~ s#[']##g;
		$txt =~ s#<[^>]+>##g;

		$txt =~ s#(_){2,}#_#g;
		$txt =~ s#(^_+|_+$)##g;

		return $txt;
	}
	sub proc1 {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:entry|bulitem|numitem|list[1-6])";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'entry') {
					$mtch = $commonPM->delattrib($mtch,'colname');
                }
				if($tag =~ /(list[1-9])/) {
					if(defined $$atthash{'dcl_flag'}) {
						$mtch = "<$$atthash{'dcl_flag'}>";
						$tmp2 = "</$$atthash{'dcl_flag'}>";
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub dclnextpara {
	my ($txt) = @_;
	$txt =~ s#~~~~/dclnextpara~~~#</dclnextpara>#g;
	$txt =~ s#~~~~dclnextpara(?: [^~]+)?~~~#<dclnextpara>#g;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:p)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'p') {
					if($tmp1 =~ /dclnextpara/) {
						while($tmp1 =~ s#<dclnextpara>(.*?)</dclnextpara>##s) {
							$tmp2 .= qq(<p>$1</p>);
						}

						if($tmp1 =~ /dclnextpara/) {
							$errorPM->error({
								type=>'fatal',
								msg=>"found unmapped dclnextpara"
							});
						}
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub listitem {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:listitem[1-6])";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		$txt = $commonPM->nesting($txt,"(?:listitem)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag =~ /^listitem/) {
					$tmp1 =~ s#<lipara>#<p>#;
					$tmp1 =~ s#</lipara>##;
					$tmp1 .= "</p>";
					$mtch =~ s#<listitem[1-6]#<li#;
					$tmp2 = "</li>";
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
}
