function call_router(sbnum) {
   if (sbnum <= 0) {
      error(FATAL,"invalid router routine number ");
   }
}
# $Id$

# $Log$

# Abstract: This program converts Rainbow or other sgml files to specific sgml/xml tagging

# Keywords: mapping Rainbow SGML

# Source: SGML                        DTD/Stylesheet: RAINBOW

# Target: SGML                      DTD/Stylesheet: (MULTIPLE)

# Project # (or Utility): 99999

# Input Prerequisites: Input must be valid SGML


# Notes:

function finish_up() {
   local mstr;
   mstr = sprintf("completion: # recs: %d, # errs: %d, # warn: %d, # info: %d, %s",
     records_read, errors,warnings,info,ctime());
   mstr = mstr " " ctime();
   print mstr >>error_log_file;
   print mstr >console_file;
   exit(highest_error_level);
}
function lookup(tag_name) {
   local j;
   if (substr(tag_name,1,1) == "^") {
      notit = TRUE;
      tag_name = substr(tag_name,2);
   } else {
      notit = FALSE;
   }
   if (substr(tag_name,1,1) == "!") {
      utag_name = substr(tag_name,2);
      for (j=istackptr; j >= 1; j--) {
         if (istack[j] == utag_name) {
            if (notit) {
               return(0);
            } else {
               return(j);
            }
         }
      }
   } else {
      for (j=stackptr; j >= 1; j--) {
         if (stack[j] == tag_name) {
            if (notit) {
               return(0);
            } else {
               return(j);
            }
         }
      }
   }
   if (notit) {
      return(1);   # was not found, but requested not found, so return true
   } else {
      return(0);   # was not found, retrun false
   }
}
function usage() {
    print "Invocation: " ARGV[0] " [optional arguments] >outfile" >console_file;
    print "Optional Arguments:" >console_file;
    for (m in params) {
       printf("-%s: %s (%s)\n",m,substr(params[m],22),substr(params[m],1,21)) >console_file;
    }
    abort(FATAL);
}
# split a tag up into an array of attribute values (the index to the array
# is the name of the attribute
function get_attributes(astr) {
   local works,j;
   works = astr;
   gsub(/[ ]+$/,"",works);
   gsub(/[ ]+[a-zA-Z0-9_\-]+[=]/,"*@&*@",works);
   num_attr = split(works,attr_parts,/[*][@]/);
#   print "number attr=",num_attr;
   if ((num_attr % 2) != 1) {
      error(ERROR,"Incorrect tag attributes");
   }
   for (j=2; j < num_attr; j+=2) {

      attr_contents = attr_parts[j+1];
      # eliminate the leading blank and = character in the attribute name
      gsub(/^[ ]+/,"",attr_parts[j]);
      attr_name = tolower(substr(attr_parts[j],1,length(attr_parts[j])-1));
      if (substr(attr_contents,1,1) ~ /["']/) {
         if (substr(attr_contents,length(attr_contents),1) ==
            substr(attr_contents,1,1)) {
            attr_contents = substr(attr_contents,2,length(attr_contents)-2);
         } else {
            if (substr(attr_contents,length(attr_contents),1) == "?") {
               if (substr(attr_contents,length(attr_contents)-1,1) ==
                  substr(attr_contents,1,1)) {
                  attr_contents = substr(attr_contents,2,length(attr_contents)-2);
               } else {
                  error(ERROR,"Mismatched tag attribute delimeters=" astr);
			   }
		  	} else {
               error(ERROR,"Mismatched tag attribute delimeters=" astr);
			}
         }
      }
      attributes[tolower(attr_name)] = attr_contents;
   }
#   next_attr = match(works,/[ ][a-zA-Z0-9_][=]/);
}
function user_initialize() {
   current_output_buffer_counter = 0;
   last_output_buffer_counter = 0;
   stackptr = 0;
   istackptr = 0;
   spec_clf_flag = TRUE;
   rbuffertag = "";
   rbuffername = "";
   holdbuff = "";
   map_file = program_arguments["m"];
   mac_count = 0;
   SOME = "-";
   ALL = "+";
   NONE = " ";
   while ((stata=getline <map_file) > 0) {
      gsub(/[;][;].+$/,"",$0);
      if (match($0,/^[ ]*synonym/)) {
         f2 = $2;
		 gsubs("`"," ",f2);
         for (c=3; c <= NF; c++) {
            ftxt = $c;
            if (ftxt ~ /^[\/]/) {
               nrepl = sub(/^[\/]/,"",ftxt);
               nrepl += sub(/[\/]$/,"",ftxt);
               if (nrepl != 2) {
   		           error(ERROR,"invalid synonym regular expression syntax, must be surrounded by '/' and not contain a space " $0);
               }
               ftxt = tolower(ftxt);
			   if (ftxt ~ /^[a-z][a-z]/) {
			      pref = substr(ftxt,1,2);
			   } else {
                  pref = "**";
			   }
               regex_synonyms[pref][ftxt] = f2;
			} else {
  			   gsubs("\\/","/",ftxt);
  			   gsubs("`"," ",ftxt);
               synonyms[ftxt] = f2;
			}
         }
         continue;
      }
      if (index($1,"attrmap")) {
         attrmap[$2][$3] = $4;
         continue;
      }
         rcount++;
         f1 = $1;
#         gsubs("`"," ",f1);
         tag = f1;
         if (tag in synonyms) {
		    error(ERROR,"tag " f1 " used in a mapping line was already a synonym to " synonyms[f1]);
         }
         if (f1 != oldf1) {
            mac_count++;
            nlines[mac_count] = 0;
         }

         macro[f1] = mac_count;
      nlines[mac_count]++;
      occur = nlines[mac_count];
      ortable[mac_count][occur] = $0;
      contains[mac_count][occur] = "";
#      acounters[mac_count][occur] = "";  now 3 dimensions
      expression[mac_count][occur] = "";
      account = 0;
      lattributes[mac_count][occur][1] = "";
      lacount = 0;
      follow_count[mac_count][occur] = 0;
      action_count[mac_count][occur] = 0;
      scall[mac_count][occur] = 0;
      for (i=2; i <= NF; i++) {
         fchar = substr($i,1,1);
         if (fchar == "(") {
            contains[mac_count][occur] = substr($i,2,length($i)-2);
         } else if (fchar == "=") {
            wrk = $i;
            gsubs("^"," ",wrk);
            lattributes[mac_count][occur][++lacount] = tolower(substr(wrk,2,length(wrk)-2));
         } else if (fchar == "+") {
            acounters[mac_count][occur][++account] = substr($i,2,length($i)-2);
         } else if (fchar ~ /[\/]/) {
            ex = $i;
            if ($i ~ /[/][+\-]?$/) {
            } else {
               lend = FALSE;
               for (q=i+1; q <= NF; q++) {
                  ex = ex " " $q;
                  if ($q ~ /[/][+\-]?$/) {
                     lend = TRUE;
                     i = q;
                     break;
                  }
               }
               if (!lend) {
                  errs = sprintf("invalid action line expression, %d=%s",rcount,$0);
                  error(FATAL,errs);
               }
            }

            works = substr(ex,2);
            if (works ~ /[+]$/) {
               works = substr(works,1,length(works)-2);
               delete_text = ALL;
            } else if (works ~ /[-]$/) {
               works = substr(works,1,length(works)-2);
               delete_text = SOME;
            } else {
               works = substr(works,1,length(works)-1);
               delete_text = NONE;
            }
            expression[mac_count][occur] = delete_text works;

# print "f1=",f1," maccount=",mac_count," occur=",occur," sc=",sc," was ",$i;
         } else if (fchar == "{") {
            break;
         }
      }
      startp = index($0,"{");
      if (startp) {
          action = substr($0,startp+1);
          endp = index(action,"}");
          if (endp <= 0) {
             errs = sprintf("invalid action line expression, (missing }) %d=%s",rcount,$0);
             error(FATAL,errs);
          }
          if (substr(action,endp+1,1) == "!") {
             reverse_endtag[mac_count][occur] = TRUE;
          } else {
             reverse_endtag[mac_count][occur] = FALSE;
          }
          action = substr(action,1,endp-1);
          if (index(action,"rmatch")) {
             workl = action;
             outstr = "";
			 gsub(/rmatch([0-9]+)/,"{{!$1!}}",workl);
			 numa = split(workl,lparts,/([{][{][!]|[!][}][}])/);
             rcount = 0;
			 for (n=2; n <= numa; n+=2) {
                rcount++;
			    outstr = outstr "|%" lparts[n] "$" lparts[n];
			 }
		     irmatch[mac_count][occur] = outstr;
		  } else {
		     irmatch[mac_count][occur] = "";
		  }
          callnum = index(action,"call_");
#          if (callnum) {
#             scall[mac_count][occur] = substr(action,callnum+5);
#          } else {
#             if (index(action,"skiptext")) {
#                scall[mac_count][occur] = -1;
#             } else {
          nump = split(action,pieces,"[<>]");
#          for (j=1; j <= nump; j++) {
#              print "line ",$0,j," pi[j]=",pieces[j],"!";
#          }
          for (j=1; j <= nump; j++) {
             if ((j % 2) == 0) {
                aval = ++action_count[mac_count][occur];
                actions[mac_count][occur][aval] = "<" pieces[j];
             } else {
                if (pieces[j] != "") {
# print "line was ",$0,"j,pi",j,">",pieces[j],"<";
                   aval = ++action_count[mac_count][occur];
                   actions[mac_count][occur][aval] = pieces[j];
#                   if (callnum) {
#                      aval = ++action_count[mac_count][occur];
#                      firstchr = match(action,/[^call\_0-9]/);
#                      actions[mac_count][occur][aval] = substr(action,firstchr);
#                      break;
#                   }
               }
             }
          }
       } else {
          errs = sprintf("invalid action line %d=%s",rcount,$0);
          error(FATAL,errs);
       }
       oldf1 = f1;
   }
   if (rcount == 0) {
      error(FATAL,"Action file contained no records");
   }
   sgml_file = program_arguments["s"];
   level = 1;
   ecount = 0;
   while ((stata=getline <sgml_file) > 0) {
      ecount++;
      if (index($0,"...")) {
         level++;
      } else if (index($0,"$$$")) {
         level = 0;
      } else if (index($0,"!!!")) {
         level = -1;
      } else {
#         $1 = tolower($1);
         if (substr($1,1,1) == "!") {
            itag_level[substr($1,2)] = level;
         } else {
            if (substr($1,1,1) == "%") {
               $1 = substr($1,2);
               do_not_close[tolower($1)] = 1;
            }
            if (xml) {
               tag_level[tolower($1)] = level;
               casedtag[tolower($1)] = $1;
			} else {
               tag_level[$1] = level;
			}
            if ($2 != "") {
               if ($2 == "NULL") {
                  otag[$1] = "";
               } else {
                  otag[$1] = $2;
               }
            } else {
               otag[$1] = $1;
            }
         }
      }
   }
   if (ecount == 0) {
      error(FATAL,"SGML structure file contained no records");
   }
   work_name = toupper(input_file);
   gsub(/[.][A-Z0-9]*$/,"",work_name);
   gsub(/^[^ ]+[\\]/,"",work_name);
   level = 1;
   icount = 0;
   sfs = FS;
   FS = "[ ][ ][ ]+";

}
function initialize() {
   local i,option,num_input,arg_string,option_type,mstr;
   # set to an extension if you want a line number file to be read
#   line_number_file_extension = "lnr";
   console_file = "CON";
   line_number_file_extension = "";
   FALSE = 0;
   TRUE = 1;
   INFO = 101;
   WARNING = 102;
   ERROR = 103;
   FATAL = 104;
   REQ = 1;
   OPT = 0;
   REQUIRED = " (required)";
   INPUT_FILE_PARAM =   "Input file argument  ";  # parameter is an input file
   OUTPUT_FILE_PARAM =  "Output file argument "; # parameter is an output file
   USER_DEFINED_PARAM = "User defined argument"; # parameter is a string which user will interpret
   FLAG_PARAM =         "Flag, no argument    ";   # parameter is simply a flag (i.e. -d, no text allowed)
   records_read = 0;
   debug = 0;
   info = 0;
   warnings = 0;
   errors = 0;
   err_string[INFO] = "* INFO * ";
   err_string[WARNING] = "* WARN * ";
   err_string[ERROR] = "* ERR * ";
   err_string[FATAL] = "* FATAL * ";
   params["o"] =  OUTPUT_FILE_PARAM "console file name";
   params["m"] =  INPUT_FILE_PARAM "map file name" REQUIRED;
   params["s"] =  INPUT_FILE_PARAM "SGML structure map file" REQUIRED;
#   params["i"] =  INPUT_FILE_PARAM "information module table" REQUIRED;
   params["b"] = USER_DEFINED_PARAM "break line number";
   params["c"] = FLAG_PARAM "stack buffer tags when output";
   params["u"] = USER_DEFINED_PARAM "numeric user defined option";
   params["v"] = USER_DEFINED_PARAM "string user defined option";
   params["l"] = FLAG_PARAM "dump line number tags in output file";
   params["d"] = FLAG_PARAM "debugging mode";
   params["t"] = FLAG_PARAM "process tables";
   params["x"] = FLAG_PARAM "Output is XML (and cases need to be case sensitive)";
   params["k"] = FLAG_PARAM "skip !doctype line";
   highest_error_level = 100;
   num_input = 0;
   for (i=1; i<= ARGC-1; i++) {
      if (substr(ARGV[i],1,1) == "-") {
         option = substr(ARGV[i],2,1);
         if (option in params) {
             option_type = substr(params[option],1,21);
             subs(REQUIRED,"",option_type);
             arg_string = substr(ARGV[i],3);
             program_arguments[option] = arg_string;
             if (option_type == FLAG_PARAM) {
               if (arg_string != "") {
                  error(ERROR,"Parameter -" option " does not take an argument");
                  usage();
               }
             } else {
                if (arg_string == "") {
                  error(ERROR,"Parameter -" option " requires an argument");
                  usage();
                }
                if (option_type == INPUT_FILE_PARAM) {
                   # check if file exists
                   ifile_size = filesize(arg_string);
                   if (ifile_size <= 0) {
                      error(ERROR,"Input file " arg_string " does not exist");
                      usage();
                   }
                }
             }
         } else {
            error(ERROR,"Invalid parameter -" option " supplied");
            usage();
         }
      } else {
         num_input++;
         if (num_input > 1) {
             error(ERROR,"More than one file name was supplied");
             usage();
         } else {
            input_file = tolower(ARGV[i]);
         }
      }
      ARGV[i] = "";
   }
   if (num_input == 0) {
       error(ERROR,"No input file name was supplied");
       usage();
   }

   for (i in params) {
      if (index(params[i],REQUIRED)) {
         if (i in program_arguments) {
         } else {
             error(ERROR,"Required parameter -" i " was not supplied");
             usage();
         }
      }
   }

   if ("d" in program_arguments) {
      debug = TRUE;
   }
   if ("o" in program_arguments) {
      console_file = program_arguments["o"];
   } else {
      console_file = "CON";
   }
   if ("k" in program_arguments) {
      skip_doctype = TRUE;
   } else {
      skip_doctype = FALSE;
   }
   if ("c" in program_arguments) {
      buffcheck = TRUE;
   } else {
      buffcheck = FALSE;
   }
   if ("x" in program_arguments) {
      xml = TRUE;
   } else {
      xml = FALSE;
   }
   if ("u" in program_arguments) {
      nuser_defined_variable = program_arguments["u"]*1;
   } else {
      nuser_defined_variable = -1;
   }
   if ("v" in program_arguments) {
      suser_defined_variable = program_arguments["v"];
   } else {
      suser_defined_variable = "";
   }
   if ("l" in program_arguments) {
      dump_lineno = TRUE;
   } else {
      dump_lineno = FALSE;
   }
   if ("b" in program_arguments) {
      break_line = program_arguments["b"];
   }
   lpos = match(input_file,/[.][a-z0-9]*$/);
   if (lpos) {
      error_log_file = substr(input_file,1,lpos) "elg";
      line_number_file = substr(input_file,1,lpos) line_number_file_extension;
   } else {
      error_log_file = input_file ".elg";
      line_number_file = input_file "." line_number_file_extension;
   }
   line_num_file = error_log_file;
   print "\n****************************" >>error_log_file;
   mstr = "Invocation: " ARGV[0] "($Revision$)" ;
   for (i=1; i <= ARGC; i++) {
      mstr = mstr " " ARGV[i];
   }
   mstr = mstr " " ctime();
   print mstr >>error_log_file;
   print mstr >console_file;
   user_initialize();
}
# get_attr_val returns the contents of a particular attribute ('attrib')
# for the current tag. The secodn parameter to the function, 'isrequired',
# is set true if the attribute should allways  be there and should
# result with an error being issued if it is not
function get_attr_val(attrib,isrequired) {
   was_null = FALSE;
   if (tolower(attrib) in attributes) {
      value = attributes[tolower(attrib)];
      return(value);
   } else {
      if (isrequired) {
          errorl(ERROR,"Required attribute '" attrib "' is missing " );
      }
      was_null = TRUE;
      return("");
   }
}


function get_input_line() {
   stata = getline <input_file;
   if (stata > 0) {
      records_read++;
   }
   if ((records_read % 50) == 0) {
      printf("R%6d\r",records_read) >console_file;
   }
   if (length($0) > 29000) {
      errorl(WARNING,"Input line may be too long, " length($0));
   }
   if (length($0) >=31998) {
      errorl(ERROR,"Input line too long, was truncated , length=" length($0));
   }
   if (line_number_file_extension != "") {
      gstat = getline original_line_number <line_number_file;
      if (gstat <= 0) {
         error(WARNING,"Could not read line number file record " records_read);
         original_line_number = 0;
      } else {
         line_map[records_read] = original_line_number;
      }
   }
   return(stata);
}
# generate an error message with line number and text information
function errorl(severity,mess) {
   if (line_number_file_extension != "") {
      linenum = line_map[records_read];
   } else {
      linenum = records_read;
   }
   bmess = sprintf("%s; line #=%d; line text=%s\n",mess,linenum,$0);
   error(severity,bmess);
}
function error(severity,mess) {
   print err_string[severity] mess >console_file;
   if (error_log_file != "") {
      print err_string[severity] mess >>error_log_file;
   }
   if (severity > highest_error_level) {
      highest_error_level = severity;
   }
   if (severity == INFO) {
      info++;
   } else if (severity == WARNING) {
      warnings++;
   } else if (severity == ERROR) {
      errors++;
   } else if (severity == FATAL) {
      exit(highest_error_level);
   }
}
function output(strng) {
   if (dumpit) {
         current_output_buffer[++current_output_buffer_counter] = strng;
         return();
   }
   gsubs("{[(","<",strng);
   gsubs(")]}",">",strng);
   gsubs("]@]","}",strng);
   gsub(/[ ][^=]+[=]["][^"]*NULLATTRIBUTE[^"]*["]/,"",strng); 
   if ((strng != "") && (skiptext == "")) {
      if (match(strng,/^[<][/]?([^ \/>]+)[ >]/,1,pstart,plength)) {
         tagn = substr(strng,pstart[1],plength[1]);
         if (tagn in otag) {
            reptag = otag[tagn];

            if (reptag == "") {
               return(0);
            } else if (reptag == tagn) {
            } else {
               strng = substr(strng,1,pstart[1]-1) reptag substr(strng,pstart[1]+plength[1]);
            }
         }
      }
      if (rbuffertag != "") {
          if (holdbuff == "") {
             holdbuff = strng;
          } else {
             holdbuff = holdbuff "\n" strng;
          }
      } else {
         current_output_buffer[++current_output_buffer_counter] = strng;
      }
   } else {
      if (skiptext != "") {
         wrk = strng;
         numq = split(wrk,wrkparts,/([\[][\[][\[])|([\]][\]][\]])/);

         if (numq > 1) {
            sstrng = "";
            for (numc=2; numc <= numq; numc += 2) {
                sstrng = sstrng "[[[" wrkparts[numc] "]]]";
            }
            current_output_buffer[++current_output_buffer_counter] = sstrng;
            error(INFO,"Outputting codes from skiptext line : " substr(sstrng,1,60));
         }
         error(INFO,"Skipping skiptext <" skiptext "> line : " substr(strng,1,60));
      }
   }
}
function ifilep() {
     numq = split(aparts[n],piic,/[,]/);
     if (piic[3] == "*") {
        piic[3] = length(work_name);
     }
     useo = substr(work_name,piic[2],piic[3]);
     if (aparts[n] ~ /[A-Z]/) {
        useo = toupper(useo);
     } else {
        useo = tolower(useo);
     }
     attributeval = useo;
}
function rcase(istr) {
   if (xml) {
      if (tolower(istr) in casedtag) {
         ostr = casedtag[tolower(istr)];
	  } else {
         ostr = istr;
	  }
   } else {
      ostr = istr;
   }
   return(ostr);
}
function put_tag(stng) {
   local work,end_tag,tag_name_pos,tag_name;
   work = stng;
   if (skiptext != "") {
      return(0);
   }
   if (substr(work,1,1) == "/") {
      end_tag = TRUE;
      work = substr(work,2);
   } else {
      end_tag = FALSE;
   }
   tag_name_pos = match(work,/[> ]/);
   if (tag_name_pos == 0) {
      tag_name_pos = length(work)+1;
   }
   # The name of the tag is fetched
   tag_name = tolower(substr(work,1,tag_name_pos-1));
   if (xml) {
      rtag_name = substr(work,1,tag_name_pos-1);
   } else {
      rtag_name = tag_name;
   }
   if (stng ~ /^[!?]/) {
      lev = -5;
   } else if (rtag_name in tag_level) {
      lev = tag_level[rtag_name];
      casedname = casetag[rtag_name];
   } else {
      casedname = rtag_name;
      errorl(ERROR,tag_name " not in SGML tag structuring file");
   }
   if (buffcheck && (rbuffertag != "")) {
      # if buffering check option is on, we will not stack tags now, but instead check later
   } else if (end_tag) {
      if (stack[stackptr] != tag_name) {
        found_end = FALSE;
        if (lev > -1) {
            if (lookup(tag_name)) {
               for (l=stackptr; l >= 1; l--) {
                   if (stack[l] == tag_name) {
                      found_end = TRUE;
                      stackptr = l-1;
                      break;
                   }
                   if (stack[l] <= 0) {
                       errorl(ERROR,"tag " stack[l] " missing required endtag");
                   }
                   if (stack[l] in do_not_close) {
             errorl(ERROR,"Unexpected problem encountered, tag " stack[l] " closing automatically");
                   }
                   if (index(stack[l],"dummy")) {
                   } else {
                      output("</" rcase(stack[l]) ">");
                   }
                }
             }
             if (found_end) {
             } else {
                errorl(ERROR,"tag /" tag_name  " not found in stack");
             }
        } else {
        }
     } else {
        stackptr--;
     }
   } else {
      if (lev > 0) {
         for (l=stackptr; l >= 1; l--) {
            slev = tag_level[stack[l]];
            if (slev <= 0) {
               errorl(ERROR,"tag " stack[l] " missing required endtag");
               break;
            }
            if (slev < lev) {

                break;
            }
            if (stack[l] in do_not_close) {
        errorl(ERROR,"Unexpected problem encountered, tag " stack[l] " closing automatically");
            }
            if (index(stack[l],"dummy")) {
            } else {
               output("</" rcase(stack[l]) ">");
            }
            stackptr--;
         }
         stack[++stackptr] = tag_name;
         fstack[stackptr] = stng;
      } else if (lev == 0) {
         stack[++stackptr] = tag_name;
         fstack[stackptr] = stng;
      } else {
      }
   }
   if (index(tag_name,"dummy")) {
   } else {
      if (xml) {
	     newname = rcase(tag_name);
		 if (newname != tag_name) {
            subs(tag_name,newname,stng);
		 }
      }
      output("<" stng ">");
   }
}
function atmap () {
                             numsp = split(aparts[n],apics,/[,]/);
                             if (apics[2] in attrmap) {
                                 attributeval = get_attr_val(apics[3],REQ);
                                 if (attributeval in attrmap[apics[2]]) {
                                    attributeval = attrmap[apics[2]][attributeval];
                                 } else {
                                    errorl(ERROR,"Attribute not in mapping table ;" attributeval ";" aparts[n]);
                                 }
                              } else {
                                 errorl(ERROR,"No such Attribute map setup; " apics[2]);
                              }
}
function chkat () {
                                           if (aparts[n] ~ /[?]$/) {
										      sub(/[?]$/,"",aparts[n]);                                           
                                              attributeval = get_attr_val(aparts[n],0);
                                              if (was_null) {
											     attributeval= "NULLATTRIBUTE";
    										  }
                                           } else {
                                              attributeval = get_attr_val(aparts[n],REQ);
										   }
}
function debugou() {
                   ssst = "\nst--[";
                   for (y=1; y <= stackptr; y++) {
                      ssst = ssst stack[y] "|";
                   }
                   ssst = ssst "]";
                   ssst = ssst " is--[";
                   for (y=1; y <= istackptr; y++) {
                      if (istack_tags[y] == "") {
                         extraj = "";
                      } else {
                         extraj = "{" istack_tags[y] "}";
                      }
                      ssst = ssst istack[y] extraj "|";
                   }
                   ssst = ssst "]";
#                   print rbuffertag ";" dumpit "MAP Line:{" orline "}" ssst >error_log_file;
                   output("MAP Line:{" orline "}" ssst);
}
function prdeb(prm) {
   rtag = prm;
   if (debug) {
     output("\nIN TAG:{" rtag "}");
   }
}
function chbr(bname) {
   if (!(bname in sbuffer)) {
      return();
   }
   wrkline = sbuffer[bname];
   if (wrkline == "") {
      return();
   }
   numl = split(sbuffer[bname],buparts,/[\n]/);
   for (b = 1; b <= numl; b++) {
      nline = buparts[b];
      if (index(nline,"<")) {
         nline = substr(nline,2,length(nline)-2);
         put_tag(nline);
	  } else {
         output(nline);
	  }
   }
}
function check_attributes() {
                   if (lat != "") {
                      numla = split(lat,laparts,/[,]/);
                      usedattr[laparts[1]] = TRUE;
                      if (numla == 1) {   # just attribute name, not value
                         if (get_attr_val(laparts[1],FALSE) == "") {
                            # attribute was not seen, continue with next action
                            if (was_null) {
                               acont = 1;
                            }
                         }
                      } else if ((numla >= 2)  && (numla <= 3)) {
                         if (numla == 3) {
                            laparts[2] = laparts[2] "," laparts[3];
                         }
                         if (substr(laparts[1],1,1) == "<") {
                            # special case for clf (not really an attribute)
                            wbline = tolower(wbuff[current_line_counter+1]);
                            if (wbline ~ /^[ ]*$/) {   # if line was empty, try the next one
                               wbline = tolower(wbuff[current_line_counter+2]);
                            }
                            if (laparts[2] == "") {
                               laparts[2] = laparts[1];
                            }
                            if (index(wbline,laparts[1]) &&
                               index(wbline,laparts[2])) {
#                               spec_clf_flag = TRUE;
#                               textm = wbuff[current_line_counter+2];
                            } else {
                               acont = 1;
                            }
                         } else if (laparts[1] == "clf") {
                            # special case for clf (not really an attribute)
                            wbline = tolower(wbuff[current_line_counter+1]);
                            if (index(wbline,"<clf") &&
                               index(wbline,laparts[2])) {
#                               spec_clf_flag = TRUE;
#                               textm = wbuff[current_line_counter+2];
                            } else {
                               acont = 1;
                            }
                         } else {
                            atval = tolower(get_attr_val(laparts[1],FALSE));
                            if (atval == "") {
                               if (was_null) {
                                  acont = 1;  # attribute was not seen, continue with next action
                               }
                            } else {
                               if (substr(laparts[2],1,1) == "/") {
                                  mt = laparts[2];
                                  gsubs("/","",mt);
                                  mt = tolower(mt);
                                  if (match(tolower(atval),mt)) {
                                     # the regexp expression matched
                                  } else {
                                     acont = 1; # attribute did not match
                                  }
                               } else if (tolower(atval) != laparts[2]) {
                                  acont = 1; # attribute did not match
                               }
                            }
                         }
                      } else {
                         error(FATAL,"Invalid action attribute check" lat);
                      }
                   }
}
function putbtext() {
   # since we are deleting text from regexp and we have not yet checked the attribute 
   # conditon, we need to put the text we deleted back if we are not selecting
   # the specific action
   if (rpartext != "") {
      partext = rpartext;
   }
   if (rpline != "") {
      wbuff[rplineno] = rpline;
   }
}
function mdefault() {
          if (paratype in macro) {
          } else {
             if ("mdefault" in macro) {
                paratype = "mdefault";
                if (paratype in missing) {
   			    } else {
                   missing[paratype] = 1;
                   errorl(INFO,paratype " not in mapping table, 'mdefault' action will be taken ");
                }
			 } 
		  }
}
function syn_lookup(tname) {
   otname = tname;
   ltname = tolower(tname);
   pre = substr(ltname,1,2);
   if (tname in synonyms) {
      otname = synonyms[tname];
   } else if (pre in regex_synonyms) {
      for (g in regex_synonyms[pre]) {
         if (match(ltname,g)) {
             otname = regex_synonyms[pre][g];
             if (debug) {
                 print "Synonym Match " tname " ~ " g " => " otname;
			 }
             break;
         }
 	  }
   }
   if ((otname == tname) && ("**" in regex_synonyms)) {
      pre = "**";
      for (g in regex_synonyms[pre]) {
         if (match(tname,g)) {
             otname = regex_synonyms[pre][g];
             if (debug) {
                 print "Synonym Match " tname " ~ " g " => " otname;
			 }
             break;
         }
 	  }
   }
   return(otname);
}
# for each sgml tag seen in the SGML input file,
# this routine is called to handle the tag.  This routine
# is constructed like a jump table (although it is in fact a series
# of if ... else if lines.  Some tags are processed directly in
# this routines, and for others, a seperate function is called.
# The input parameter, 'str', contains the contents of the current tag.
# This routine is mainly driven by the contents of the ECFRM.DAT file,
# which is read in at the beginning of prcoessing.
function process_tag(str) {
   local l,m,o;
   local end_tag;
   local rftag;
   local yes_action;
   local work;
   work = str;
   extranow = FALSE;
   dobua = FALSE;
   iempty = FALSE;
   prdeb(str);
   if (substr(work,1,1) == "/") {
      end_tag = TRUE;
      work = substr(work,2);
   } else {
      end_tag = FALSE;
   }
   tag_name_pos = match(work,/[> ]/);
   if (tag_name_pos == 0) {
      tag_name_pos = length(work)+1;
   }
   # The name of the tag is fetched
   tag_name = tolower(substr(work,1,tag_name_pos-1));
   rtag_name = tag_name;
   
   tag_name = syn_lookup(tag_name);
   if (skiptext != "") {
      if (end_tag) {
        if (((!skiptype) && (tag_name ~ /^(sub)?para$/) && (skiptext == istack[istackptr])) ||
           ((tag_name == skiptext) && (tag_name == istack[istackptr]))) {
           if (dumpit) {
              output("<" str ">");
           }
           istackptr--;
           skiptext = "";
           dumpit = FALSE;
           return(0);
        } else {
           if (dumpit) {
              output("<" str ">");
           }
           return(0);
        }
      } else {
         if (dumpit) {
            output("<" str ">");
         }
         return(0);
      }
   }
   if (!end_tag) {
      # get the attributes of the tag
      if (index(str,"=")) {
         delete attributes;
         get_attributes(substr(work,tag_name_pos));
      } else {
         delete attributes;
      }
   }
   skip_ist = 0;
   if (tag_name in macro) {
       id = macro[tag_name];
       if (index(actions[id][1][1],"empty")) {
           skip_ist = TRUE;
           iempty = TRUE;
       } else if (index(actions[id][1][1],"dumptag")) {
           skip_ist = TRUE;
       } else if (index(actions[id][1][1],"skiptag")) {
           skip_ist = TRUE;
       }
   }
   if (end_tag < 999) {  # dummy if for now, is allways TRUE
      if (end_tag) {
#        if (skiptext == istack[istackptr]) {
#            if (!ignore) {
#               output("<" str ">");
#            } else {
#               ignore = 0;
#            }
#
#            skiptext = "";
#         }
         if (tag_name == "clf") {
            spec_clf_flag = FALSE;
         }
         if (!skip_ist) {
            if (rbuffername != "") {
               if (rbuffertag == istack[istackptr]) {
                  dobua = TRUE;
               }
            }
            if (tag_name == "para") {
            } else if (tag_name == "subpara") {
            } else {
               if (istack[istackptr] != tag_name) {
                   errorl(ERROR,"end tag/istack mismatch (" tag_name ") expecting " istack[istackptr]);
               }
            }
            end_tag_list = istack_tags[istackptr];
            if (end_tag_list == "") {
            } else {
               numend = split(end_tag_list,end_tag_parts,/[!]/);
               for (i=numend-1; i >= 1; i--) {
                   put_tag("/" end_tag_parts[i]);
               }
            }
            istack_tags[istackptr] = "";   # weve flipped it, clear it out
            paratype = "^" istack[istackptr];
            if (paratype in macro) {
               process = TRUE;
            } else {
               process = FALSE;
            }
            istackptr--;
         } else {
            process = TRUE;
            paratype = "^" tag_name;
         }
      } else {
         # see if the tag is mentioned in the tag action table that
         # we read in at beginning of processing
         if (index(str,"paratype=")) {
             paratype = get_attr_val("paratype",TRUE);
             paratype = syn_lookup(paratype);
             gsubs(" ","`",paratype);
             if (paratype == "") {
                paratype = "emptyparatype";
             }
             realtag = FALSE;
         } else if (tag_name == "clf") {
             paratype = "clf";
             realtag = TRUE;
         } else {
             paratype = tag_name;
             realtag = TRUE;
         }
         if (!skip_ist) {

            istackptr++;
            istack[istackptr] = paratype;
         } else {
         }
         process = TRUE;
      }
   }
   opartext = partext;
   if (process) {
      if (paratype != "") {
          mdefault();
          if (paratype in macro) {
             id = macro[paratype];
             num_lines = nlines[id];
             yes_action = FALSE;
             for (m=1; m <= num_lines; m++) {
                remove = FALSE;

                # check if the sgml tag contained condition is true
                cont = contains[id][m];
                orline = ortable[id][m];
                if (cont  != "") {
                   numc = split(cont,contparts,/[,]/);
                   lookitup = TRUE;
                   for (o=1; o <= numc; o++) {
                      nconp = contparts[o];
                      if (index(nconp,"|")) {
                          numor = split(nconp,nconpar,/[\|]/);
                          look2 = TRUE;
                          for (s=1; s <= numor; s++) {
                             if (lookup(nconpar[s])) {
                                look2 = FALSE;
                                break;
                             }
                          }
                          if (look2) {
                             lookitup = FALSE;
                             break; # skip the action, it is not conatined within text
                          }
                      } else if (lookup(nconp)) {
                      } else {
                         lookitup = FALSE;
                         break; # skip the action, it is not conatined within text
                      }
                   }
                   if (!lookitup) {
                      continue; # one of the tags was not found, go on to next rule
                   }
                }
                xbreak = 0;
                for (x in acounters[id][m]) {
                   # check if a counter condition is true
                   ocount = acounters[id][m][x];
                   if (ocount  != "") {
                      qcsp = split(ocount,qcsplit,/[,]/);
                      if (qcsp != 3) {
                         errorl(ERROR,"Invalid count check expression " ocount);
                      } else {
                         if (qcsplit[2] ~ /[0-9]+/) {
                         } else {
                            errorl(ERROR,"Invalid count check expression (p2) " ocount);
                         }
                         if (qcsplit[3] ~ /[0-9]+/) {
                         } else {
                            errorl(ERROR,"Invalid count check expression (p3) " ocount);
                         }
                         if (qcsplit[1] in counters) {
                            gvalu = counters[qcsplit[1]];
                            if ((gvalu >= qcsplit[2]) && (gvalu <= qcsplit[3])) {
                            } else {
                               xbreak = 1;
                               break;
                               # we are not in range, don't do action
                            }
                         } else {
                            errorl(ERROR,"Count variable has not been set  " ocount);
                            xbreak = 1;
                            break;
                         }
                      }
                   }
                }
                if (xbreak == 1) {
                   xbreak = 0;
                   continue;
                }

                delete rmatch;
                rpartext = "";
				rpline = "";
                # if there is a regular expression condition, check it now
                expr = expression[id][m];
                if (expr  != "") {
                   regexp = substr(expr,2);
                   if (substr(expr,1,1) == NONE) {
                      remove = FALSE;
                   } else if (substr(expr,1,1) == SOME) {
                      remove = TRUE;
                      extranow = TRUE;
                   } else {
                      remove = TRUE;
                   }
                   if (match(regexp,/^@s/)) {
                      regexp = substr(regexp,3);
                      flags = "";
                   } else {
                      flags = "i";
                   }
                   if (match(regexp,/^@p/)) {
                      regexp = substr(regexp,3);
                      param = TRUE;
                   } else {
                      param = FALSE;
                   }
                   spempty = FALSE;
                   ptr = regex(regexp,flags);
                   if (paratype == "clf") {
                      nextlin = wbuff[current_line_counter+1];
                      pos = match(nextlin,ptr);
                   } else if (((!realtag) && (paratype == "para")) || param) {
                      pos=match(partext,ptr);
                   } else {
                      lint = 0;
                      for (tt=current_line_counter+1; tt <= wbuff_count; tt++) {
                         if (substr(wbuff[tt],1,1) == "<") {
                            if (substr(wbuff[tt],1,2) == "</") {
							   break;
							} else {
							
                               if (end_tag) break;  # if current tag is an endtag,dont look for match past a start tag
                            } 
                         } else {
                            lint = tt;
                            break;
                         }
                      }
                      if (lint > 0) {
                         nextlin = wbuff[lint];

                         pos = match(nextlin,ptr);

                      } else {
                         nextlin = "";
                         spempty = TRUE;
                         pos = match(nextlin,ptr);  # need to check for empty
						      # regexp which could match
                      }
                   }
                   if (pos) {
                      rpartext = partext;
                      if (remove) substr(partext,pos,RLENGTH) = "";
                         if (spempty) {
						 } else {
                         for (tt=current_line_counter+1; tt <= wbuff_count; tt++) {
                            pline = wbuff[tt];
                            if (substr(pline,1,1) == "<") {
                               if (end_tag && (substr(pline,1,2) != "</")) break;
                               continue;
							}
                            post = match(pline,ptr);
                            rworkvar = "";
                            if (post) {
                               workvar = substr(pline,post,RLENGTH);
                               rworkvar = workvar;
                               if (remove) {
                                  rpline = pline;
								  rplineno = tt;
							      substr(pline,post,RLENGTH) = "";
							   }
#                               lchar = "";
#                               numpar = 0;
#                               left = 0;
#                               for (f=1; f <= length(regexp); f++) {
#                                  nchar = substr(regexp,f,1);
#                                  if (nchar =="(") {
#                                     if (lchar != "\\") {
#                                        left++;
#                                     }
#                                  } else if (nchar == ")") {
#                                     if (lchar != "\\") {
#                                        left--;
#                                     }
##                                     if (left == 0) {
#                                        numpar++;
##                                     }
#                                  }
#                                  lchar = nchar;
#                               }
                               delete rmatch
#                               if (numpar > 0) {
#                                  ostr = "$1";
#                                  for (o=2; o <= numpar; o++) {
#                                     ostr = ostr "|" o "$" o;
#                                  }
                               if (irmatch[id][m] != "") {
                                  gsub(ptr,irmatch[id][m],workvar);
                                  numsp = split(workvar,rmatcha,/[|][%]/);
                                  for (o=2; o <= numsp; o++) {
                                     rmatch[substr(rmatcha[o],1,1)] = substr(rmatcha[o],2);
                                  }
#                                     for (o=1; o <= numpar; o++) {
#                                        if (o in rmatch) {
#                                        } else {
#                                           rmatch[o] = "";
#                                        }
                               } else {
#									 delete rmatch;
 							   }
#                               } else {
#                                  delete rmatch;
#                               }
                               if (rworkvar != "") {
                                  rmatch[0] = rworkvar;
                               }
                               wbuff[tt] = pline;
                               break;
                            }
                         }
                         }
                         if (partext ~ /[A-Za-z]/) {
                            leftover = partext;
                            gsub(/^[ ]+/,"",leftover);
                            gsub(/[ ]+$/,"",leftover);
                            partext = leftover;
                         }
                   } else {
                      remove = FALSE;
                      continue;  # expression did not match, continue
                   }
                }

                acont = 0;
                excl = FALSE;
                delete usedattr;
                for (d in lattributes[id][m]) {
                   acont = 0;
                   # if there is an attribute condition, check it now
                   lat = tolower(lattributes[id][m][d]);
                   if (lat == "exclusive") {
                      excl = TRUE;
                      for (i in attributes) {
					     usedattr[i] = FALSE;
				      }
				      continue;
				   }
                   check_attributes();
                   if (acont) {
                      break;
                   }
                }
                if (acont) {
                   putbtext();
                   continue;
                }
                if (excl) {
                   for (i in usedattr) {
				       if (usedattr[i]) {
					   } else {
                          # there were other attributes, not exclusive, so continue
                          acont = 1;
                          break;
					   }
				   }
				}
                if (acont) {
                   putbtext();
                   continue;
                }

                # at this point, a condition must have been satisfied, now
                # perform the actions
                if (debug) {
                   debugou();
                }
                # see if the end tag output ("!" at and of actions) is set
                if (reverse_endtag[id][m]) {
                   build_tags = TRUE;
                   build_tag_string = "";
                } else {
                   build_tags = FALSE;
                }
                yes_action = TRUE;
                continuev = FALSE;
                ftag = id;
                # loop down list of actions
                for (l=1; l <= action_count[ftag][m]; l++) {
                    naction = actions[ftag][m][l];
# print "l",l," naction=",naction,"<",curr_tag;
                    if (substr(naction,1,1) == "<") {
                       if (naction == "<dummy") {
                          continue;
                       }
                       conditional = FALSE;
                       if (match(naction,/[?]$/)) {
                          conditional = TRUE;
                          gsub(/[?]$/,"",naction);
                       }

                       if (substr(naction,2,1) == "/") {
                          start_tag = "/";
                          tag_part = substr(naction,3);
                          tattributes = "";
                       } else {
                           start_tag = "";
                           naction = substr(naction,2);
                           fspace = index(naction," ");
                           if (fspace) {
                              tag_part = substr(naction,1,fspace-1);
                              tattributes = " " substr(naction,fspace+1);
                              if (index(tattributes,"[[")) {
                                 numat = split(tattributes,aparts,/([\[][\[])|([\]][\]])/);
                                 oattributes = "";
                                 for (n=1; n <= numat; n++) {
                                     if ((n % 2) == 1) {
                                        oattributes = oattributes aparts[n];
                                     } else {
                                        if (aparts[n] == "paratext") {
                                           attributeval = opartext;
                                           remove = TRUE;
                                        } else if (aparts[n] == "nextline") {
                                           nextlin = wbuff[current_line_counter+1];

                                           if (substr(nextlin,1,1) == "<") {
                                              errorl(ERROR,"Invalid Nextline operator " nextlin);
                                           } else {
                                              attributeval = nextlin;
                                              wbuff[current_line_counter+1] = "";
                                           }
                                        } else if (aparts[n] == "rparatext") {
                                           attributeval = partext;
                                           remove = TRUE;
                                        } else if (tolower(substr(aparts[n],1,9)) == "ifilename") {
                                           ifilep();
                                        } else if (substr(aparts[n],1,8) == "ucounter") {
                                           commap = index(aparts[n],",");
                                           usen = substr(aparts[n],commap+1);
                                           if (index(usen,",")) {
                                              commap = index(usen,",");
                                              uformat = substr(usen,commap+1);
                                              usen = substr(usen,1,commap-1);
                                           } else {
                                              uformat = "%d";
                                           }
                                           if (usen in counters) {
                                              attributeval = sprintf(uformat,counters[usen]);
                                           } else {
                                              if (index(aparts[n],"?@!")) { # dummy line
                                              } else {
                                                 errorl(ERROR,"Invalid counter use : " usen);
                                              }
                                           }
                                        } else if (substr(aparts[n],1,6) == "rmatch") {
                                           if (substr(aparts[n],7,1) in rmatch) {
                                              attributeval = rmatch[substr(aparts[n],7,1)];
                                           } else {
                                              errorl(ERROR,"Rmatch value not available " aparts[n]);
                                           }
                                        } else if (substr(aparts[n],1,7) == "ubuffer") {
                                           commap = index(aparts[n],",");
                                           usen = substr(aparts[n],commap+1);
                                           if (usen in sbuffer) {
                                              attributeval = sbuffer[usen];
                                              if (substr(aparts[n],8,1) == "!") {
                                              } else {
                                                 delete sbuffer[usen];
                                              }
                                           } else {
										      attributeval = "";
                                              if (index(aparts[n],"?")) {
                                              } else {
                                                 errorl(ERROR,"Invalid buffer use : " usen);
                                              }
                                           }
                                        } else if (substr(aparts[n],1,7) == "attrmap") {
                                           atmap();

                                        } else {
                                           chkat();
                                        }
                                        oattributes = oattributes attributeval;
                                     }
                                 }
                                 tattributes = oattributes;

                              }
                           } else {
                              tag_part = naction;
                              tattributes = "";
                           }
                       }

                       if (conditional) {
                          if (start_tag == "/") {
                             if (lookup(tag_part)) {
                                put_tag(start_tag tag_part tattributes);
                             }
                          } else {
                             if (lookup(tag_part)) {
                             } else {
                                put_tag(start_tag tag_part tattributes);
                             }
                          }
                       } else {
                          if (build_tags) {
                             if (tag_part ~ /^[!?]/) {
							 } else {
							    build_tag_string = build_tag_string tag_part "!";
                             }
                          }
                          put_tag(start_tag tag_part tattributes);
                       }
                    } else if (substr(naction,1,5) == "call_") {
                        cnum = substr(naction,6)*1;
                        rftag = ftag;
                        call_router(cnum);
                        ftag = rftag;
                    } else  if (match(naction,/^[\[][\[]/)) {
                       numat = split(naction,aparts,/([\[][\[])|([\]][\]])/);
                       output(get_attr_val(aparts[2],OPT));
                    } else  if (match(naction,/^rbuffer[?]?[,]/)) {
                        if (index(naction,"?")) {
                           rbuffertag = "?";
                        } else {
                           rbuffertag = paratype;
                        }
                        commap = index(naction,",");
                        rbuffername = substr(naction,commap+1);
                        if (debug) {
                            print "\nStarting buffer " rbuffername;
   				        }
                    } else  if (match(naction,/^rcounter[,]/)) {
                        numq = split(naction,countparts,/[,]/);
                        if (numq != 3) {
                           error(ERROR,"Invalid rcounter action" naction);
                        }
                        if (countparts[3] ~ /^[\-]?[0-9]+$/) {
                           counters[countparts[2]] = countparts[3]*1;
                        } else if (substr(countparts[3],1,1) == "%") {
                           output(sprintf(countparts[3],counters[countparts[2]]));
                        } else if (countparts[3] == "++") {
                           if (countparts[2] in counters) {
                           } else {
                              error(WARNING,"Unitialized counter " naction);
                           }
                           counters[countparts[2]]++;
                        } else if (countparts[3] == "--") {
                           if (countparts[2] in counters) {
                           } else {
                              error(WARNING,"Unitialized counter " naction);
                           }
                           counters[countparts[2]]--;
                        } else {
                           error(ERROR,"Invalid rcounter action (part 3)" naction);
                        }

                    } else  if (match(naction,/^cbuffer[\?]?[,]/)) {
                        commap = index(naction,",");
                        usen = substr(naction,commap+1);
                        if (debug) {
                            print "\nClosing buffer " usen;
   				        }
                        if (usen in sbuffer) {
                           sbuffer[usen] = sbuffer[usen] "\n" holdbuff;
                        } else {
                           sbuffer[rbuffername] = holdbuff;
                        }
                        if (rbuffername == "") {
                           if (index(naction,"?")) {
                           } else {
                              errorl(ERROR,"Invalid buffer close (not open) " naction);
                           }
                        }
                        holdbuff = "";
                        rbuffertag = "";
                        rbuffername = "";
                    } else  if (match(naction,/^ebuffer[\?]?[,]/)) {
                        commap = index(naction,",");
                        usen = substr(naction,commap+1);
                        if (usen in sbuffer) {
                           delete sbuffer[usen];
                        }
                    } else  if (match(naction,/^ubuffer[\?\!]*[,]/)) {
                        commap = index(naction,",");
                        usen = substr(naction,commap+1);
                        if (usen in sbuffer) {
                            if (buffcheck && (rbuffertag =="")) {
                               chbr(usen);
							} else {
                               if (debug) {
                                  print "\nUsing buffer " usen;
							   }
                               output(sbuffer[usen]);
							}
                            if (index(naction,"!")) {
                            } else {
                               delete sbuffer[usen];
                            }
                        } else {
                             if (index(naction,"?")) {
                             } else {
                                errorl(ERROR,"Invalid buffer use : " usen);
                             }
                        }
                    } else  if (naction == "clearpartext") {
                        partext = "";
                    } else  if (substr(naction,1,6) == "rmatch") {
                       if (substr(naction,7,1) in rmatch) {
                          output(rmatch[substr(naction,7,1)]);
                       } else {
                          errorl(ERROR,"Rmatch value not available " naction);
                       }
                    } else  if (naction == "empty") {
                    } else  if (naction == "dumptoendtag") {
                       skiptext = paratype;
					   skiptype = realtag;
                       dumpit = 1;
                       ignore = 0;
                       output("<" str ">");
                    } else  if (naction == "skiptext") {
                       skiptext = paratype;
                       skiptype = realtag;
                       ignore = 0;
#                       output("<" str ">");
                    } else  if (naction == "ignoretoendtag") {
                       skiptype = realtag;
                       ignore = 2;
                       skiptext = paratype;
                    } else  if (naction == "dumptag") {
                       output("<" str ">");
                    } else  if (naction == "skiptag") {
                    } else  if (naction == "continue") {
                       continuev = TRUE;
                    } else  if (naction == "stacktag") {
                       put_tag(str);
                    } else {
                       output(naction);
                    }
                }
                if (!continuev) {
                   break;  # that is it for this pattern
                }
             }
             if (!yes_action) {
                if (!end_tag) {
                   errorl(ERROR,"<" str "> not satisfied in action table");
                   output("<" str ">");
                }
             }
          } else {
             errorl(ERROR,paratype " not in mapping table" "(orig was <" parts[k] ">)");
             output("<"  parts[k] ">");
          }

      } else if (tag_name == "clf") {
#      } else if (tag_name == "table") {
#         skiptext = tag_name;
#         output("<" str ">");
      } else {
         output(parts[k]);
         errorl(ERROR,"Unknown rainbow tag");
      }
      if ((!end_tag) && (!skip_ist)) {
         if (build_tags) {
            istack_tags[istackptr] = build_tag_string;
         } else {
            istack_tags[istackptr] = "";
         }
      }
   }
   end_processing();   # cause routine is too long, we move procesing down below
}
function end_processing() {
   if (extranow) {
      if (leftover != "") {
#         output(leftover);
         leftover = "";
      }
      extranow = FALSE;
   }
   if (iempty && (rbuffername != "")) {
      if (rbuffertag == tag_name) {
         dobua = TRUE;
      }
   }

   if (dobua) {
      if (rbuffername in sbuffer) {
#          errorl(ERROR,"sbuffer entry " rbuffername " was never used " sbuffer[rbuffername]);
         sbuffer[rbuffername] = sbuffer[rbuffername] "\n" holdbuff;
      } else {
         sbuffer[rbuffername] = holdbuff;
      }
      holdbuff = "";
      rbuffertag = "";
      rbuffername = "";
   }
   build_tags = FALSE;
}
function dump_buffer() {
   local l;
   srecords_read = records_read;
   for (current_line_counter=1; current_line_counter <= wbuff_count;
      current_line_counter++) {
      records_read = lines_wbuff[current_line_counter];
      if ((records_read % 25) == 0) {
         printf("P%6d    \r",records_read) >console_file;
      }
      if (dump_lineno) {
          output("<linetag no=\"" records_read "\">");
      }
      if (records_read == break_line) {
         print "At break line " break_line >console_file;
      }
      $0 = wbuff[current_line_counter];
      current_line = $0;
          if (substr(current_line,1,1) != "<") {
              # text here
              if (skiptext == "junk1235") {  # temporary turned off
                 output(current_line);
              } else if (remove) {
                 if (suser_defined_variable == "") {
                     output(current_line);
                 }
              } else {
                 if ((leftover != "") && (current_line ~ /[^ ]/)) {
#                    output("<leftover>");
#                    output(leftover);
#                    output("</leftover>");
                    leftover = "";
                 }
                 output(current_line);
              }
          } else {
              # tags here
              if (skiptext == "junk1235") {
#                 output(current_line);
              } else {
                 process_tag(substr(current_line,2,length(current_line)-2));
              }
#              if ("</" skiptext ">" == current_line) {
#                 skiptext = "";
#              }
          }
   }
   remove = FALSE;
   records_read = srecords_read;
   if (last_output_buffer_counter > 0) {
      # dump current output buffer
      for (l=1; l <= last_output_buffer_counter; l++) {
         print last_output_buffer[l];
      }
      last_output_buffer_counter = 0;

#      delete last_output_buffer;
   }
   if (current_output_buffer_counter > 0) {
      # update last output buffer with current output buffer
      for (l=1; l <= current_output_buffer_counter; l++) {
         last_output_buffer[l] = current_output_buffer[l];
      }
      last_output_buffer_counter = current_output_buffer_counter;
#      delete current_output_buffer;
      current_output_buffer_counter = 0;
   }
}
function update_buffer() {
   local i,filtr;
#   delete wbuff;
   partext = "";
   for (i=1; i <= cbuff_count; i++) {
      filtr = cbuff[i];
      if (substr(filtr,1,6)  == "<para ") {
         partext = "";
      }
      if (substr(filtr,1,1)  != "<") {
         if (length(partext) < 2000) {
             partext = partext filtr;     # the actual text in the paragraph
         }
      }
      wbuff[i] = cbuff[i];
      lines_wbuff[i] = lines_cbuff[i];
      if (wbuff[i] == "</clf>") {
         if (substr(wbuff[i-1],1,4) == "<clf") {
            wbuff[i-1] = "";  # empty clf pair is meaningless
            wbuff[i] = "";
         }
      }

   }
   wbuff_count = cbuff_count;
   wbuff[wbuff_count+1] = "";
   wbuff[wbuff_count+2] = "";
   wbuff[wbuff_count+3] = "";
#   delete cbuff;
   cbuff_count = 0;
}
BEGIN {
   initialize();
   yes_linetag = FALSE;
   first = FALSE;
   dumpit = FALSE;
   while (get_input_line() > 0) {
      if (skip_doctype && (substr($0,1,2) == "<!")) {  # this needs to be fixed, quick for alex
         parts[1] = " " $0;
         nump = 1;
      } else {
         nump = split($0,parts,/[<>]/);
      }
      if ((nump % 2) != 1) {
         errorl(ERROR,"Invalid number of angle brackets on source line");
      }
      if ($0 ~ /[^\x20-\x7e]/) {
#         gsub(/[^\x20-\x7e]/
#        errorl(WARNING,"Line contained NON-ASCII characters");
      }
      etag = "";
      for (k=1; k <= nump; k++) {
          if ((k % 2) == 1) {
              # text here
              if (parts[k] != "") {
                 cbuff[++cbuff_count] = parts[k];
                 if (yes_linetag) {
                    lines_cbuff[cbuff_count] = precords_read;
                 } else {
                    lines_cbuff[cbuff_count] = records_read;
                 }
              }
          } else {
              if (index(parts[k],"linetag")) {
                 pp = split(parts[k],jparts,/["]/);
                 yes_linetag = TRUE;
                 precords_read = jparts[2];
                 continue;
              }
              cbuff[++cbuff_count] = "<" parts[k] ">";
              etag = tolower(parts[k]);
              if (yes_linetag) {
                 lines_cbuff[cbuff_count] = precords_read;
              } else {
                 lines_cbuff[cbuff_count] = records_read;
              }
              if (cbuff[cbuff_count] == "</clf>") {
                 if (substr(cbuff[cbuff_count-1],1,4) == "<clf") {
                    cbuff_count -= 2;  # empty clf pair is meaningless
                 }
              }
          }
      }
      if (substr(etag,1,1) == "/") {
         usetag = substr(etag,2);
      } else {
         usetag = "";
      }
#      print "DDD",usetag,"==>",$0 >stderr;
      if (index($0,"</para") || (usetag in itag_level) || (cbuff_count > 350000)) {
#         print "EEE" >stderr;
         if (first) {
            update_buffer();
            first = TRUE;
         } else {
            dump_buffer();
            update_buffer();
         }
      }
   }
   if (records_read == 0) {
      error(FATAL,"No input records were read");
   }
   # handle the last buffer to be processed
   dump_buffer();
   update_buffer();
   dump_buffer();
   # flush the tag stack
   for (j=stackptr; j >= 1; j--) {
      put_tag("/" stack[j]);
   }
   # flush the last output buffer
   for (l=1; l <= last_output_buffer_counter; l++) {
      print last_output_buffer[l];
   }
   # flush the current output buffer
   for (l=1; l <= current_output_buffer_counter; l++) {
      print current_output_buffer[l];
   }
}
END {
  for (q in sbuffer) {
     if (q ~ /[A-Za-z0-9]/) {
        errorl(ERROR,"sbuffer entry " q " was never used " sbuffer[q]);
     }
  }
  finish_up();
}
# !MAP ind2.map
# synonym   dcl-para Body`no`indent Body Body`Hang Body`hang NormalParagraphStyle Table`text Table`col`heads Numlist`bold`lead Body`Text No`paragraph`style
# synonym   dcl-parainline dclinline
# synonym   dcl-donothing anchor
# synonym   dcl-section1 Subhead dclsec1 Subhead`B Heading`1 heading`1 
# synonym   dcl-semiital Subhead`semi`ital
# synonym   dcl-section2 dclsec2 Heading`2 heading`2
# synonym   dcl-empty pgbrk cr
# synonym   dcl-emptyspace tab
# synonym   dcl-skip sect z_header
# synonym   dcl-figgrp Figure`name
# synonym   dcl-ref Table`References
# synonym   dcl-bultext1 bullet`text
# synonym   dcl-numtext1 Numbered`text1 numbered`text1
# synonym   dcl-bultext2 bullet`text2
# synonym   dcl-numtext2 Numbered`text numbered`text
# synonym   dcl-list2ol List
# synonym   dcl-list3ol List`2
# synonym   dcl-title Title
# synonym   dcl-fn footnote`text
# styinfo   {skiptext}
# Normal    {</topic?><topic?><title>}!
# dcl-title {</septopic?><septopic?><title>}!
# dcl-para  {<para inline="N">}!
# dcl-parainline  {<para inline="Y">}!
# dcl-donothing   {}!
# dcl-section1    {</section1?><section1?><title>}!
# dcl-semiital    {</section1?><section1?><title><i>}!
# dcl-section2    {</section2?><section2?><title>}!
# dcl-figgrp      {<para><fig-group><title>}!
# dcl-empty  {empty}
# dcl-emptyspace {empty<dummy> }
# dcl-skip  {skiptext}
# dcl-ref   {<tableref>}!
# clf =exclusive= =font-weight,Bold= =font-slant,Ital=  {<b><i>}!
# clf =exclusive= =vertical-offset,-s= =font-slant,Ital=  {<i><sub>}!
# clf =exclusive= =font-slant,Ital=   {<i>}!
# clf =exclusive= =font-weight,Bold=  {<b>}!
# clf =exclusive= =clftype,Bold=   {<b>}!
# clf =exclusive= =vertical-offset,+s=   {<sup>}!
# clf =exclusive= =vertical-offset,-s=   {<sub>}!
# clf =exclusive= =font-slant,Ital= =vertical-offset,+s=   {<sup><i>}!
# clf =exclusive= =lowercase-display,SmallCaps=   {<smallcaps>}!
# clf =exclusive= =lowercase-display,FullCaps=   {<dclfullcaps>}!
# clf =exclusive= =score-location,under= =score-type,single=  {<u>}!
# clf =exclusive= =score-location,under= =score-type,single= =font-slant,Ital= {<u><i>}!
# clf =exclusive= =score-location,under= =score-type,single= =font-slant,Ital= =font-weight,Bold= {<u><i><b>}!
# title   {<title>}!
# hyperlink =rid= {<xref href="[[rid]]" format="html" scope="external">}!
# dcl-fn   {<para><fn>}!
# table {stacktag}!
# colspec	{dumptag}
# tgroup {</title?>stacktag}
# thead {stacktag}
# tbody {stacktag}
# tfoot {stacktag}
# row {stacktag}
# entry {stacktag}
# entrypara {stacktag}
# dcl-numtext1 {<list1 dcl_flag="ol"?></listitem1?><listitem1?><lipara>}!
# dcl-bultext1  {<list1 dcl_flag="ul"?></listitem1?><listitem1?><lipara>}!
# dcl-numtext2 {<list2 dcl_flag="ol"?></listitem2?><listitem2?><lipara>}!
# dcl-bultext2  {<list2 dcl_flag="ul"?></listitem2?><listitem2?><lipara>}!
# dcl-list2ol  {<list2 dcl_flag="ol"?></listitem2?><listitem2?><lipara>}!
# dcl-list3ol  {<list3 dcl_flag="ol"?></listitem3?><listitem3?><lipara>}!
# !TBL ind2.tbl
# septopic
# ...
# section1
# ...
# section2
# ...
# section3
# ...
# table
# fig-group
# title
# para
# ...
# tgroup
# ...
# thead
# tbody
# tfoot
# ...
# row
# ...
# entry
# ...
# entrypara
# ...
# list1
# ...
# listitem1
# ...
# list2
# ...
# listitem2
# ...
# list3
# ...
# listitem3
# ...
# lipara
# ...
# dclnoinline
# tableref
# ...
# xref
# fn
# $$$
# i
# b
# sub
# sup
# smallcaps
# dclfullcaps
# u
# !!!
# colspec
