#!perl

use strict;
use sigtrap;
use diagnostics;

use File::Basename;
use File::Path;
use File::Copy;
use Cwd;
use Getopt::Long::Descriptive;
use Data::Dump qw(dump);
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Smart::Comments;
use utf8;
use Roman;
$|=1;
binmode STDOUT, ":utf8";
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my $ver = "1.3";			#version number of the program
my $revdate = "12042007";		#date of latest revision
my $Version = $ver."-".$revdate;	#the combination of the two will display in the ELG file
my $commonPM = filter::common->new('common');
my $errorPM;

my $OLDSIGWARN = $SIG{__WARN__};
local $SIG{__WARN__} = sub {
	my $message = shift;
	local $| = 1;
	#&$OLDSIGWARN($message);
	my $e = log::error->new("Error Log File","programming_error.elg",$Version);
	$e->error({type=>'fatal',msg=>"$message"});
};


{
my $outfile;
my ($filename,$filename_ext);
my ($opt, $usage);
my @InputFiles = ();
my %SETUP;
my %PROC;
my %REFTAG;
my (%COUNTER,%XREFENT);
my %counter = ();
my %TFOOTHASH = ();


	sub initialize {
		#Type declarations are '=x' or ':x',
		#where = means a value is required and : means it is optional.
		#x may be 's' to indicate a string is required, 'i' for an integer, or 'f' for a number with a fractional part.
		#The type spec may end in @ to indicate that the option may appear multiple times.
		($opt, $usage) = describe_options(
			'%c %o <some-arg>',
			[ 'test',   "test flag"],
			[ 'input=s', "input file", { required => 1  } ],
			[ 'output=s', "input file", { required => 1  } ],
			[ 'help',       "print usage message and exit" ],
		);

		print($usage->text), exit if $opt->help;
	}
	sub setup {
		#set vars, and whatever
		#push @InputFiles, $opt->input;
		my $path = cwd();
		foreach my $option (sort(keys(%$opt))) {
			$SETUP{'opt_'.$option} = $opt->$option;
		}
	}

	sub finish_up { 		#close elg and exit program cleanly
		#exit( $highest_error_level );
	}
	#-------------------------------------------------------------------------------

	&initialize(); 	# routine called at start of processing
	&setup();

	push @InputFiles,$SETUP{'opt_input'};
	&processfile(); #new 04.18.2013
	#-------------------------------------------------
	sub processfile {
		foreach my $input_file (@InputFiles) {
			($filename = $input_file) =~ s#([.])[^.]+$##s;
			$filename_ext = $2;
			#-----------Elg------------------
			my $elg = $filename;
			#$elg =~ s#^.*?[\\/]([^\\/]+)$#$1#s;
			$elg .= '.elg';
			$errorPM = log::error->new("Error Log File","$filename.elg",$Version);
			#$elg = $SETUP{'home'}.'\\'.$elg;
			if (-r $input_file) {
				my $text = io($input_file)->utf8->slurp;
				$text = &procline($text);

				#need more options here
				io($SETUP{'opt_output'})->utf8->write($text);
				$errorPM->finish(1);
			}
			else {
				$errorPM->error({
					type=>'fatal',
					msg=>"could not read $input_file"
				});
			}
		}
	}
	sub procline {
	my ($txt) = @_;

		my $dir = lc(getcwd());
		my ($tab,$unit) = $dir =~ m#active/([^/]+)/([^/]+)/work#;

		if($tab =~ /^[56]$/) {
			$txt = &clean($txt);
			$txt = &proc1($txt);

			$txt = &nextpara($txt);
			$txt = &proc2($txt);
			$txt = &proc3($txt);
			$txt = &dclligrp($txt);

		}


		return( $txt );
	}
	sub  cleanlistpara {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para)";
	my %hash = ();
	my %type2level;
	my $curlvl = 0;
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {
					if(defined $$atthash{'dcllist'}) {
						#$mtch = $commonPM->delattrib($mtch,"dclmark");
						#$mtch = $commonPM->delattrib($mtch,"dclbullet");
						#$mtch = $commonPM->delattrib($mtch,"dcllisttype");
						#$mtch = $commonPM->delattrib($mtch,"dcllvl");
						#$mtch = $commonPM->delattrib($mtch,"dcllist");
						$mtch = $commonPM->addattribvalue($mtch,"paratype","list".$$atthash{'dcllvl'});
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub  listpara {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para)";
	my %hash = ();
	my %type2level;
	my $curlvl = 0;
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {

					if(defined $$atthash{'dcllist'}) {
						if(not defined $type2level{$$atthash{'dcllisttype'}}) {
							$type2level{$$atthash{'dcllisttype'}} = ++$curlvl;
						}
						my $blt = "";
						if($$atthash{'dcllisttype'} >= 6) {
							$blt = "ul";
						} else {
							$blt = "ol";
						}
						$mtch = $commonPM->addattribvalue($mtch,"dcllvl",$type2level{$$atthash{'dcllisttype'}}.$blt)
					} else {
						$errorPM->error({
							type=>'fatal',
							msg=>"para should be a list. [[[$mtch$tmp1$tmp2]]]"
						});
					}
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub dclligrp {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:dclligrp)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'dclligrp') {
					$tmp1 = listpara($tmp1);
					$tmp1 = cleanlistpara($tmp1);
					$mtch = $tmp2 = "";
				}
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub proc3 {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para)";
	my %hash = ();
	my $lasttype = 0;
	my $lastmarker = "";
	my $lastid=0;
	my %setroman;
	my $newlist = 0;
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {
					if(defined $$atthash{'dclbullet'}) {

						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list1") if $$atthash{'dcllisttype'} eq '1';
						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list2") if $$atthash{'dcllisttype'} eq '2';
						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list3") if $$atthash{'dcllisttype'} eq '3';
						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list4") if $$atthash{'dcllisttype'} eq '4';
						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list5") if $$atthash{'dcllisttype'} eq '5';
						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list6") if $$atthash{'dcllisttype'} eq '6';
						$mtch = $commonPM->addattribvalue($mtch,"dcllist","list7") if $$atthash{'dcllisttype'} eq '7';

						if($newlist == 0) {
							$newlist = 1;
							$mtch = qq(<dclligrp>$mtch);
						}
						my $len = 150;
						if(length $tmp3 < 150) {
							$len = length $tmp3;
						}
						my ($tt) = $tmp3 =~ m#^(.{$len})#s;
						my ($uu) = $tt =~ m#(<[^>]+>?)#s if $tt;
						if($uu) {
							if($uu !~ /dclbullet/) {
								$tmp2 .= qq(</dclligrp>);
								$newlist = 0;
							}
						}

					}

                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub proc2 {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para)";
	my %hash = ();
	my $lasttype = 0;
	my $lastmarker = "";
	my $lastid=0;
	my %setroman;
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {
					if(defined $$atthash{'dclbullet'}) {
						if($$atthash{'dclmark'} =~ m~[a-z]+~) {
							#if($lasttype == 1 and ($lastmarker == "" or isroman($lastmarker)) and isroman($$atthash{'dclmark'})) {
							#	$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","1");
							#}
							$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","1");
							$lasttype = 1;
						}
						if($$atthash{'dclmark'} =~ m~[A-Z]+~) {
							$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","2");
							$lasttype = 2;
						}
						if($$atthash{'dclmark'} =~ m~[0-9]+~) {
							$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","3");
							$lasttype = 3;
						}
						if($$atthash{'dclmark'} =~ /^(i{1,3}|i?vi{0,3}|i?xi{0,3})$/i) {  #isroman($$atthash{'dclmark'})
							if($lastmarker !~ /[hu]/ and $$atthash{'dclmark'} =~ /[ivx]/) {
								$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","4");
								$mtch = $commonPM->addattribvalue($mtch,"isroman","4");
								$lasttype = 4;
							} elsif($lastmarker !~ /[HU]/ and $$atthash{'dclmark'} =~ /[IVX]/) {
								$mtch = $commonPM->addattribvalue($mtch,"isroman","5");
								$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","5");
								$lasttype = 5;
							}
						}
						if($$atthash{'dclmark'} =~ m~&#x02022;~) {
							$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","6");
							$lasttype = 6;
						}
						if($$atthash{'dclmark'} =~ m~&#x02023;~) {
							$mtch = $commonPM->addattribvalue($mtch,"dcllisttype","7");
							$lasttype = 7;
						}
						$lastmarker = $$atthash{'dclmark'};
						$lastid++;
					} else {
						#$lastmarker = "";
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub proc1 {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {
					if(defined $$atthash{'paratype'} and $$atthash{'paratype'} !~ /(Figure space|Figure name|Subhead|dclsec|Heading|Figure Labels)/i) {
						if($tmp1 =~ m~^[\s]*(?:<clf [^>]+>\n*)?(&#x02022;|&#x02023;|\s+[o]\s+|\s*[a-zA-Z0-9]{1,3}\.\s|\s*[a-zA-Z0-9]+\)\s|\s*\([a-zA-Z0-9]+\)(?:\s+|$))~) {
							my $marker = $1;
							$mtch = $commonPM->addattribvalue($mtch,"dclul","0");
							if($marker =~ /\s+[o]\s+/) {
								$marker = "&#x02023;";
								$tmp1 =~ s#\s+o\s+##;
							}
							$marker =~ s#(\n|^\s+|\s+$|\(|\)|[.])##g;
							$mtch = $commonPM->addattribvalue($mtch,"dclmark",$marker);
							$mtch = $commonPM->addattribvalue($mtch,"dclbullet","1");
						}
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub nextpara {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:para)";
	my %hash = ();
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'para') {

					if(defined $$atthash{'dclbullet'}) {
						($tmp1,$tmp3) = isnextindented($mtch,$tmp1,$tmp3);
						if($tmp3 =~ /DCLNEXTPARA/) {
							$errorPM->error({
								type=>'fatal',
								msg=>"'DCL NEXT PARA' para not correctly replaced"
							});
						}
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
	sub isnextindented {
		my ($mtch,$tmp1,$tmp3) = @_;

		while($tmp3 =~ s#^(\s*<para(?: [^>]+)>.*?</para>)#DCLNEXTPARA#s) {
			my ($nextpara) = ($1);
			if($nextpara =~ / (dclbullet)="/) {
				$tmp3 =~ s#DCLNEXTPARA#$nextpara#;
				return ($tmp1,$tmp3)
			} else {
				$nextpara =~ m#<para(?: [^>]+)?>(.*?)</para>#s;
				my $t1 = $1;
				if($t1 =~ /^\n*[ ]+/ and $nextpara =~ /paratype="(Body no indent|Body|Body Hang|Body hang|NormalParagraphStyle|Table text|Table col heads|Numlist bold lead|Body Text|No paragraph style|Head D|Parameters)"/i) {
					$nextpara =~ s#<(/?)para((?: [^>]+)?)>[ ]*#~~~~$1dclnextpara$2~~~#g;
					$tmp1 .= $nextpara;
					$tmp3 =~ s#DCLNEXTPARA##;
				} else {
					$tmp3 =~ s#DCLNEXTPARA#<DCLNONNEXTPARA>$nextpara#;
				}
			}
		}

		$tmp3 =~ s#<DCLNONNEXTPARA>##g;


		return ($tmp1,$tmp3);
	}
	sub clean {
	my ($txt) = @_;
	my ($lft,$mtch,$newtxt) = ("","","");
	my $tagxpr = "(?:clf|para)";
	my %hash = ();
	#my ($progress) = $txt =~ s#</$tagxpr>#$&#g;
		#$txt = $commonPM->nesting($txt,"(?:section|subsec)");
		while ($txt =~ m#<($tagxpr(?:[_][0-9]+)?)(?: [^>]+)?>#s) {
			$lft = $`;
			$mtch = $&;
			$txt = $';
			my $tag = $1;
			my $atthash = $commonPM->getattribs($mtch);
			if ($txt =~ m#</$tag>#si) {
				my $tmp1 = $`;
				my $tmp2 = $&;
				my $tmp3 = $';
				#print $progress--," " x 6,"\r";
				if ($tag eq 'clf') {
					if($tmp1 =~ m~^\n*(&#x02022;|^\n*[o]\n*|[a-z0-9][.]\s*$)\n*$~) {
						$mtch = $tmp2 = "";
						$tmp3 =~ s#^\n+##;
						$lft =~ s#\n+$##;
						$tmp1 =~ s~^(\n*)[o](\n*)$~$1&#x02023;$2~;
					}
                }
				if ($tag eq 'para') {
					if($tmp1 =~ s~^(\s*\([a-zA-Z0-9]+\)\s*)(\([a-zA-Z0-9]+\)\s+)~$2~) {
						my $mark1 = $1;
						$mtch = $mtch.$mark1.$tmp2."\n".$mtch;
					}
                }
				$txt = $tmp1.$tmp2.$tmp3;
			}
			$newtxt .= $lft. $mtch;
        }
		$txt = $newtxt . $txt;
	return( $txt );
	}
}
