#!perl

use IO::All;
use Data::Dump qw(dump);
my %styles;
my $cnt = 0;
foreach $f (<*/work/*.elg>) {
    my $text = io($f)->utf8->slurp;
    @lines = split /\n/, $text;
    foreach $l (@lines) {
        if($l =~ /^[*] ERR [*] [^;]+/) {
            my $match = $&;
            $styles{$match} = $f;
            print ++$cnt,"\r";
        }
    }
}

foreach $k (keys %styles) {
    io("W:/DEV/Users/Benjamin/p27463-conv/reports/elg.rpt")->utf8->append("$k\t$styles{$k}\n");
}