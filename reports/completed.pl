#!perl

use File::Basename;
use File::Path;
use File::Copy;
use File::NCopy;
use File::Find;
use Cwd;
use IO::All;
use lib 'y:\sw\lib\perl';
	use log::error;
	use filter::common;
use Storable qw(store retrieve);
use Data::Dump qw(dump);
$|=1;
my $commonPM = filter::common->new('common');
my $DEBUG = ($0 =~ /.exe/ or not $ENV{USERID} eq 'Benjamin') ? 0 : 1;
my $dm = sub {$DEBUG and print "\n\n$_\n" foreach @_;print "\nPaused. Hit <enter> to continue..."if$DEBUG;my$c="";$c=<>if$DEBUG;$DEBUG=0if $c eq "stop\n"};

my %DOCTYPES;
my %SAMPLETYPE;
my %UNITS;

my @InputFiles;
my $load = 1;
my $listhash = 'W:/DEV/Users/Benjamin/p27463-conv/reports/fulldocx.hash';
open my $DOC, ">", "X:/DEV/Users/Benjamin/p27463-conv/reports/completed.txt";
my (%DOCX2XML,%DOCX2XML2);

if($load) {
	my $DIR = cwd();
	my $cnt = 0;
	find(\&origfolders, "k:/p27463/active");
	sub origfolders {
		$cnt++;
		my $d = $File::Find::dir;
        return if $d !~ m#active/([1-6])/([^/]+)#;
        my ($tab,$doc) = ($1,$2);
        $DOCX2XML{$tab}{$doc}{docx}="" if not defined $DOCX2XML{$tab}{$doc}{docx};
        $DOCX2XML{$tab}{$doc}{dir}=$d if not defined $DOCX2XML{$tab}{$doc}{dir};
		return if $_ !~ /[.](xml|docx)$/;
        if($d =~ /work$/) {
            if($_ =~ /[.]docx$/) {
                $DOCX2XML{$tab}{$doc}{docx}= $_;
                $DOCX2XML{$tab}{$doc}{dir}= $d;
            }
            if($_ =~ /[.]xml$/) {
                push @{$DOCX2XML{$tab}{$doc}{xmls}}, $_;
            }
        } elsif ($d =~ /source$/){

            if($_ =~ /[.]docx$/) {
                 #&$dm($d,$_,"~~~$DOCX2XML2{$tab}{$doc}{docx}~~~");
                $DOCX2XML{$tab}{$doc}{docx}= $_ if $DOCX2XML{$tab}{$doc}{docx} eq "";
            }
        }

		#my $o = $_;
		#$o =~ s#[.]xml#.org#;
		#return unless -e $o;
		print $cnt,"\r";
		#push @InputFiles, "$d/$_";
	}
	chdir $DIR;

	unlink $listhash if -e $listhash;
	store \%DOCX2XML, $listhash;
} else {
	%DOCX2XML = retrieve($listhash);
}
my $c=0;

foreach my $k (sort {$a<=>$b} keys %DOCX2XML) {
    #print $DOC "\n$k";

    my %docs = %{$DOCX2XML{$k}};
    #dump(%docs);
    #&$dm();
    foreach my $d (sort keys %docs) {
        print $DOC "\n$k\t$docs{$d}{docx}\t$docs{$d}{dir}";
        foreach my $x (@{$docs{$d}{xmls}}) {
            print $DOC "\t$x";
        }
        #dump($docs{$d});
        #&$dm@{$docs{$d}{xmls}}();
    }
    #dump(%docs);

}


close $DOC;


exit;
